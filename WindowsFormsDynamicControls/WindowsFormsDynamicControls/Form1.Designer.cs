﻿namespace WindowsFormsDynamicControls
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.showpicture = new System.Windows.Forms.Button();
            this.width = new System.Windows.Forms.NumericUpDown();
            this.heigth = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.branch = new System.Windows.Forms.NumericUpDown();
            this.draw = new System.Windows.Forms.Button();
            this.xmas = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.heigth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xmas)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Width:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Height:";
            // 
            // showpicture
            // 
            this.showpicture.Location = new System.Drawing.Point(19, 115);
            this.showpicture.Name = "showpicture";
            this.showpicture.Size = new System.Drawing.Size(105, 23);
            this.showpicture.TabIndex = 4;
            this.showpicture.Text = "Bild anzeigen";
            this.showpicture.UseVisualStyleBackColor = true;
            this.showpicture.Click += new System.EventHandler(this.showpicture_Click);
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(63, 27);
            this.width.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.width.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(61, 20);
            this.width.TabIndex = 5;
            this.width.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // heigth
            // 
            this.heigth.Location = new System.Drawing.Point(63, 66);
            this.heigth.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.heigth.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.heigth.Name = "heigth";
            this.heigth.Size = new System.Drawing.Size(61, 20);
            this.heigth.TabIndex = 6;
            this.heigth.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Branch:";
            // 
            // branch
            // 
            this.branch.Location = new System.Drawing.Point(63, 201);
            this.branch.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.branch.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.branch.Name = "branch";
            this.branch.Size = new System.Drawing.Size(61, 20);
            this.branch.TabIndex = 8;
            this.branch.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // draw
            // 
            this.draw.Location = new System.Drawing.Point(19, 240);
            this.draw.Name = "draw";
            this.draw.Size = new System.Drawing.Size(105, 23);
            this.draw.TabIndex = 9;
            this.draw.Text = "Zeichnen";
            this.draw.UseVisualStyleBackColor = true;
            // 
            // xmas
            // 
            this.xmas.BackColor = System.Drawing.SystemColors.Control;
            this.xmas.Image = ((System.Drawing.Image)(resources.GetObject("xmas.Image")));
            this.xmas.Location = new System.Drawing.Point(176, 29);
            this.xmas.Name = "xmas";
            this.xmas.Size = new System.Drawing.Size(100, 100);
            this.xmas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.xmas.TabIndex = 10;
            this.xmas.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 502);
            this.Controls.Add(this.xmas);
            this.Controls.Add(this.draw);
            this.Controls.Add(this.branch);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.heigth);
            this.Controls.Add(this.width);
            this.Controls.Add(this.showpicture);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "XMAS TREE";
            ((System.ComponentModel.ISupportInitialize)(this.width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.heigth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xmas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button showpicture;
        private System.Windows.Forms.NumericUpDown width;
        private System.Windows.Forms.NumericUpDown heigth;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown branch;
        private System.Windows.Forms.Button draw;
        private System.Windows.Forms.PictureBox xmas;
    }
}

