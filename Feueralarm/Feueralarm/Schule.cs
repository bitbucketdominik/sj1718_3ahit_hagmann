﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feueralarm
{
    class Schule
    {
        public delegate void fire(string typ);
        public event fire Fire_;

        public Schule()
        {

        }

        public int GetPlatzNummer(Klasse k)
        {
            switch(k.Name)
            {
                case "3BHITT":
                    return 1;
                case "2BHITT":
                    return 2;
                default:
                    return 5;
            }
        }

        public void StartFeueralarm(string typ)
        {
            Fire_(typ);
        }
    }
}
