﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feueralarm
{
    class Schueler
    {
        private string name;
        private Klasse klasse;
        private Schule schule;

        public Schueler(string n, Schule s, Klasse k)
        {
            name = n;
            klasse = k;
            schule = s;

            s.Fire_ += s_fire;
        }

        private void s_fire(string typ)
        {
            Console.WriteLine(typ + " !!! Schüler " + name + " begibt sich in den Hof " + schule.GetPlatzNummer(klasse));
        }
    }
}
