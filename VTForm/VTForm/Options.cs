﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Windows.Forms;

namespace VTForm
{
    class Options
    {
        string path;

        private static Options _instance;

        protected Options (string path)
        {
            this.path = path;
        }

        public Grade txtread()
        {
            StreamReader sr = null;
            //string dirvalue = sr.ReadLine();
            //string wordvalue = sr.ReadLine();
            Grade grades = new Grade("X");
            //System.IO.FileNotFound

            try
            {
                sr = new StreamReader(path);
                //throw new ObjectDisposedException("ObjectDisposedException");
                //throw new ArgumentException();
            }
            catch (FileNotFoundException e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("FileNotFoundException", e);
            }
            catch (ArgumentNullException ane)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("ArgumentNullException", ane);
            }
            catch (ArgumentException ae)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("ArgumentException", ae);
            }

            catch (DirectoryNotFoundException dnfe)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("DirectoryNotFoundException", dnfe);
            }

            catch (IOException ioe)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("IOException", ioe);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }
            List<string> alist = new List<string>();



            try
            {
                while (!sr.EndOfStream)
                {
                    alist.Add(sr.ReadLine());
                }
                //throw new IOException();
            }
            catch (ObjectDisposedException ode)
            {
                throw new VTException("ObjectDisposedException", ode);
            }
            catch (OutOfMemoryException oome)
            {
                throw new VTException("OutOfMemoryException", oome);
            }
            catch (IOException ioe)
            {
                throw new VTException("IOException", ioe);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }

            try
            {
                grades.dirvalue = alist[0];
                //throw new Exception();
            }
            catch (ArgumentOutOfRangeException aoore)
            {
                throw new VTException("ArgumentOutOfRangeException", aoore);
            }
            catch (Exception e)
            {
                throw new VTException("Exception", e);
            }

            try
            {
                grades.wordvalue = Convert.ToInt32(alist[1]);
                //throw new OverflowException();
            }
            catch (FormatException fe)
            {
                throw new VTException("FormatException", fe);
            }
            catch (OverflowException oe)
            {
                throw new VTException("OverflowException", oe);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }


            int t = alist.Count - 3;

                int s = t / 3;

            try
            {

                for (int i = 3; i < s + 3; i++)
                {
                    if (alist[i] == "!")
                    {
                        continue;
                    }
                    else
                    {
                        grades.percent.Add(Convert.ToDecimal(alist[i]));
                    }
                }

            }

            catch (FormatException fe)
            {
                throw new VTException("FormatException", fe);
            }
            catch (OverflowException oe)
            {
                throw new VTException("OverflowException", oe);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }

            for (int i = s + 3; i < 2 * s + 3; i++)
                {
                    if (alist[i] == "!")
                    {
                        continue;
                    }
                    else
                    {
                        grades.sentence.Add(Convert.ToString(alist[i]));
                    }
                }


            try
            {
                for (int i = 2 * s + 3; i < 3 * s + 3; i++)
                {
                    if (alist[i] == "!")
                    {
                        continue;
                    }
                    else
                    {
                        grades.value.Add(Convert.ToDecimal(alist[i]));
                    }
                }
            }
            catch (FormatException fe)
            {
                throw new VTException("FormatException", fe);
            }
            catch (OverflowException oe)
            {
                throw new VTException("OverflowException", oe);
            }
            catch (Exception e)
            {
                throw new VTException("Exception", e);
            }
            sr.Close();
                //return grades;
            //}
            return grades;
        }

        public void txtwrite(Grade grades)
        {
            try
            {
                File.WriteAllText(path, String.Empty);
                //throw new EncoderFallbackException();
            }
            catch (ArgumentNullException ane)
            {
                throw new VTException("ArgumentNullException", ane);
            }
            catch (ArgumentException ae)
            {
                throw new VTException("ArgumentException", ae);
            }
            catch (PathTooLongException ptle)
            {
                throw new VTException("PathTooLongException", ptle);
            }
            catch (DirectoryNotFoundException dnfe)
            {
                throw new VTException("DirectoryNotFoundException", dnfe);
            }
            catch (IOException ioe)
            {
                throw new VTException("IOException", ioe);
            }
            catch (UnauthorizedAccessException uae)
            {
                throw new VTException("UnauthorizedAccessException", uae);
            }
            catch (NotSupportedException nse)
            {
                throw new VTException("NotSupportedException", nse);
            }
            catch (System.Security.SecurityException se)
            {
                throw new VTException("SecurityException", se);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }

            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(path);
                //throw new Exception();
            }
            catch (System.Security.SecurityException se)
            {
                throw new VTException("SecurityException", se);
            }
            catch (ArgumentNullException ane)
            {
                throw new VTException("ArgumentNullException", ane);
            }
            catch (ArgumentException ae)
            {
                throw new VTException("ArgumentException", ae);
            }
            catch (PathTooLongException ptle)
            {
                throw new VTException("PathTooLongException", ptle);
            }
            catch (DirectoryNotFoundException dnfe)
            {
                throw new VTException("DirectoryNotFoundException", dnfe);
            }
            catch (IOException ioe)
            {
                throw new VTException("IOException", ioe);
            }
            catch (UnauthorizedAccessException uae)
            {
                throw new VTException("UnauthorizedAccessException", uae);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }


            try
            {

                sw.WriteLine(grades.dirvalue);
                sw.WriteLine(grades.wordvalue);
                sw.WriteLine("!");

                foreach (decimal percentforeach in grades.percent)
                {
                    sw.WriteLine(percentforeach);
                }
                sw.WriteLine("!");
                foreach (string sentenceforeach in grades.sentence)
                {
                    sw.WriteLine(sentenceforeach);
                }
                sw.WriteLine("!");
                foreach (decimal valueforeach in grades.value)
                {
                    sw.WriteLine(valueforeach);
                }
                sw.WriteLine("!");

                //throw new ObjectDisposedException("ObjectDisposedException");
            }
            catch (ObjectDisposedException ode)
            {
                throw new VTException("ObjectDisposedException", ode);
            }
            catch (IOException ioe)
            {
                throw new VTException("IOException", ioe);
            }
            catch (Exception e)
            {
                //throw new System.IO.FileNotFoundException();
                throw new VTException("Exception", e);
            }

            ////sw.WriteLine(String.Empty);
            //foreach (double percentforeach in percentl)
            //{
            //    sw.WriteLine(percentforeach);
            //}
            ////sw.WriteLine(String.Empty);
            //foreach (string gradeforeach in gradel)
            //{
            //    sw.WriteLine(gradeforeach);
            //}
            ////sw.WriteLine(String.Empty);
            //foreach (double valueforeach in valuel)
            //{
            //    sw.WriteLine(valueforeach);
            //}

            try
            {

                sw.Flush();
                //throw new EncoderFallbackException();
            }
            catch (ObjectDisposedException ode)
            {
                throw new VTException("ObjectDisposedException", ode);
            }
            catch (IOException ioe)
            {
                throw new VTException("IOException", ioe);
            }
            catch (EncoderFallbackException efe)
            {
                throw new VTException("EncoderFallbackException", efe);
            }
            catch (Exception e)
            {
                throw new VTException("Exception", e);
            }

            try
            {
                sw.Close();
            }
           
                catch (EncoderFallbackException efe)
            {
                throw new VTException("EncoderFallbackException", efe);
            }
        
        }

        public static Options Instance(string path)
        {
            if(_instance == null)
            {
                _instance = new Options(path);
            }

            return _instance;
        }
    }
}
