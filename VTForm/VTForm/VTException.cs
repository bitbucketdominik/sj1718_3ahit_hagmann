﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace VTForm
{
    [Serializable]
    public class VTException : ApplicationException
    {
        public VTException() { }
        public VTException(string message) : base(message) { }
        public VTException(string message, Exception inner) : base(message, inner)
        {
            //StreamWriter swe = null;
            try
            {
                using (StreamWriter swe = new StreamWriter("vt_errors.log", true))
                {
                    swe.WriteLine(DateTime.Now + " - " + inner.Message);
                }
                //throw new EncoderFallbackException();
                //swe = new StreamWriter("vt_errors.log");
                //swe.WriteAtEnd(DateTime.Now + " - " + inner.Message);
                //swe.Flush();
                //swe.Close();

                //throw new IOException();
            }
            catch (EncoderFallbackException efe)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: " + efe.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "ENCODER FALLBACK ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (ObjectDisposedException ode)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: " + ode.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "OBJECT DISPOSED", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (UnauthorizedAccessException uae)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: " + uae.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "NO ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (ArgumentNullException ane)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine ArgumentNullException ist aufgetreten: " + ane.Message +  "  Mit dem Drücken auf OK beenden Sie das Programm!", "ARGUMENT NULL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (DirectoryNotFoundException dnfe)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine ArgumentNullException ist aufgetreten: " + dnfe.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "DIRECTORY NOT FOUND", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (ArgumentException ae)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine ArgumentException ist aufgetreten: " + ae.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "ARGUMENT EXCEPTION", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (PathTooLongException ptle)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine PathTooLongException ist aufgetreten: " + ptle.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "PATH TOO LONG", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (IOException ioe)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine Eingabe-Ausgabe-Ausnahme ist aufgetreten: " + ioe.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "IO ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (System.Security.SecurityException se)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine Security-Exception ist aufgetreten: " + se.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "SECURITY ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ein Fehler ist aufgetreten beim Schreiben der Errorlogdatei: Eine Allgemein-Exception ist aufgetreten: " + ex.Message + "  Mit dem Drücken auf OK beenden Sie das Programm!", "SECURITY ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
            finally
            {
                MessageBox.Show("Ein Fehler ist aufgetreten: " + message + " | Mit dem Drücken auf OK beenden Sie das Programm!", "FATAL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                Application.Exit();
            }
        }
    }
}
