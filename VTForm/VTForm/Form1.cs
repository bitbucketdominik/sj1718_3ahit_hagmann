﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VTrainer;

namespace VTForm
{
    public partial class Form1 : Form
    {
        public Form1(string[] args)
        {
            InitializeComponent();
        }





        List<Label> ll = new List<Label>();
        List<TextBox> txl = new List<TextBox>();
        static VTrainer.Vokabeltrainer vt = new Vokabeltrainer();
        List<Vokabel> vtl;



        static Options opt2 = Options.Instance("options.txt");
        //static ArrayList arr2;
       
        //static string dirvalue2;
        //static string wordvalue2;
        //static int wordvalue2for;

        public void newTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Grade grades4 = null;
            //try
            

                grades4 = opt2.txtread();
            //}
                //MessageBox.Show("Exception ausgelöst!" + ex.Message);
            

            panel2.Controls.Clear();

            panel2.Location = new Point(200, 200);

            ll.Clear();

            txl.Clear();

            

            if (vtl != null)
                vtl.Clear();
            vt = new Vokabeltrainer();
            vtl = vt.GetList();
            grades4 = opt2.txtread();

            //arr2 = opt2.txtread();
            //dirvalue2 = (string)arr2[0];
            //wordvalue2 = (string)arr2[1];
            //wordvalue2for = Convert.ToInt32(wordvalue2);


            if (grades4.dirvalue == "EN-DE")
            {
                for (int j = 0; j < grades4.wordvalue; j++)
                {
                    ll.Add(new Label { Text = vtl[j].English });

                    txl.Add(new TextBox { Tag = vtl[j].English });
                }



                //int countll = 70;
                //int counttxl = 70;

                //for (int i = 0; i < 10; i++)
                //{
                //    ll[i].Location = new Point(70, countll);
                //    panel1.Controls.Add(ll[i]);
                //    countll = countll + 30;
                //}

                //for (int i = 0; i < 10; i++)
                //{
                //    txl[i].Location = new Point(220, counttxl);
                //    panel1.Controls.Add(txl[i]);
                //    counttxl = counttxl + 30;
                //}

                for (int i = 0; i < grades4.wordvalue; i++)
                {
                    panel2.Left = 60;
                    panel2.Top = 60;
                    ll[i].Width = 250;
                    txl[i].Width = 250;
                    panel2.Controls.Add(ll[i]);
                    panel2.Controls.Add(txl[i]);
                }
            }

            if (grades4.dirvalue == "DE-EN")
            {
                for (int j = 0; j < grades4.wordvalue; j++)
                {
                    ll.Add(new Label { Text = vtl[j].German });

                    txl.Add(new TextBox { Tag = vtl[j].German });
                }

                for (int i = 0; i < grades4.wordvalue; i++)
                {
                    panel2.Left = 60;
                    panel2.Top = 60;
                    ll[i].Width = 250;
                    txl[i].Width = 250;
                    panel2.Controls.Add(ll[i]);
                    panel2.Controls.Add(txl[i]);
                }
            }

                this.panel2.AutoScroll = true;

            this.Controls.Add(panel2);

            toolStripStatusLabel1.Text = "Dictionary Entries: " + Convert.ToString(vtl.Count());

            //for (int i = 0; i < 10; i++)
            //{
            //    panel2.Controls.Add(txl[i]);
            //}
        }

        public void button1_Click(object sender, EventArgs e)
        {

            Grade grades4 = opt2.txtread();
            //List<TextBox> txl1 = new List<TextBox>();

            //for (int k = 0; k < ll.Count; k++)
            //{
            //    if((string)txl[k].Tag == ll[k].Text)
            //    {
            //        if (txl[k].Text == vtl[k].German)
            //        {
            //            txl[k].BackColor = Color.Green;
            //        }
            //        else
            //        {
            //            txl[k].BackColor = Color.Red;
            //        }
            //    }
            //}

            int rightamount = 0;

            if (grades4.dirvalue == "EN-DE")
            {

                for (int o = 0; o < txl.Count; o++)
                {
                    if ((string)txl[o].Tag == ll[o].Text)
                    {
                        if (vt.Check(txl[o].Text, (string)txl[o].Tag) == true)
                        {
                            txl[o].BackColor = Color.Green;
                            rightamount++;
                        }
                        else
                        {
                            txl[o].BackColor = Color.Red;
                        }
                    }
                }
            }
            if (grades4.dirvalue == "DE-EN")
            {
                for (int o = 0; o < txl.Count; o++)
                {
                    if ((string)txl[o].Tag == ll[o].Text)
                    {
                        if (vt.Check((string)txl[o].Tag, txl[o].Text) == true)
                        {
                            txl[o].BackColor = Color.Green;
                            rightamount++;
                        }
                        else
                        {
                            txl[o].BackColor = Color.Red;
                        }
                    }
                }
            }
            else
            {

            }



            Double percentos = (Double)rightamount / (Double)txl.Count;



            double percent100 = percentos * 100;

            double diff = 0;
            double diffhelp = Convert.ToDouble(grades4.percent[0]) - percent100;
            int pos = 0;

            for (int i = grades4.percent.Count; i > 0; i--)
            {
                diff = Convert.ToDouble(grades4.percent[i-1]) - percent100;

                if(diff<=diffhelp && diff>=0)
                {
                    diffhelp = diff;
                    pos = i-1;
                }
            }



            MessageBox.Show("Sie haben " + Convert.ToString(percent100) + " % richtig! Sie haben die Note ***" + grades4.sentence[pos] + "*** erreicht! Herzlichen Glückwunsch zu dieser Note! ", "Notenauswertung des Formulares", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings settings = new Settings();
            settings.ShowDialog();
        }
    }
}
