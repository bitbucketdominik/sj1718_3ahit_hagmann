﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTForm
{
    class SingleGrade
    {
        public decimal percent;
        public string sentence;
        public decimal value;

        public SingleGrade (decimal percent, string sentence, decimal value)
        {
            this.percent = percent;
            this.sentence = sentence;
            this.value = value;
        }
    }
}
