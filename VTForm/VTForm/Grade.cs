﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VTForm
{
    class Grade
    {
        public string dirvalue;
        public int wordvalue;
        public List<decimal> percent = new List<decimal>();
        public List<string> sentence = new List<string>();
        public List<decimal> value = new List<decimal>();

        public Grade (string dirvalue)
        {
            this.dirvalue = dirvalue;
        }

        //public override string ToString()
        //{
        //    return "Liste von Grades";
        //}
    }
}
