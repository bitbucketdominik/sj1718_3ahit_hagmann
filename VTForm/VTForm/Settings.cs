﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VTrainer;
using System.Collections;

namespace VTForm
{
    public partial class Settings : Form
    {
        static VTrainer.Vokabeltrainer vt = new Vokabeltrainer();
        List<Vokabel> vtl = vt.GetList();

        public Settings()
        {
            InitializeComponent();
            this.direction.Items.Add("DE-EN");
            this.direction.Items.Add("EN-DE");
            Options opt3 = Options.Instance("options.txt");
            Grade grades1 = opt3.txtread();
            this.direction.SelectedItem = grades1.dirvalue;
            this.maxwords.Maximum = Convert.ToDecimal(vtl.Count);
            try
            {
                this.maxwords.Value = Convert.ToDecimal(grades1.wordvalue);
                //throw new ArgumentOutOfRangeException();
            }
            catch (ArgumentOutOfRangeException aoore)
            {
                throw new VTException("ArgumentOutOfRangeException", aoore);
            }
            catch (Exception e)
            {
                throw new VTException("Exception", e);
            }
            for (int i = 0; i < grades1.sentence.Count; i++)
            {

                try
                {
                    grades.Rows.Add();
                    grades.Rows[i].Cells["Percent"].Value = grades1.percent[i];
                    grades.Rows[i].Cells["Grade"].Value = grades1.sentence[i];
                    grades.Rows[i].Cells["Value"].Value = grades1.value[i];

                    //throw new ArgumentException();
                }
                catch (InvalidOperationException inoe)
                {
                    throw new VTException("InvalidOperationException", inoe);
                }
                catch (ArgumentOutOfRangeException aoore)
                {
                    throw new VTException("ArgumentOutOfRangeException", aoore);
                }
                catch (ArgumentException ae)
                {
                    throw new VTException("ArgumentException", ae);
                }
                catch (Exception e)
                {
                    throw new VTException("Exception", e);
                }
            }
        }

        private void savebtn_Click(object sender, EventArgs e)
        {

            //while (true)
            //{
            //grades.Columns["Percent"].CellType = Double;
            //grades_ColumnHeaderMouseClick(savebtn, e);
            //}
            Options opt4 = Options.Instance("options.txt");
            Grade grades2 = new Grade("X");
            Grade grades3 = opt4.txtread();
            //ArrayList arr1 = opt4.txtread();
            //string dirvalue1 = (string)arr1[0];
            //string wordvalue1 = (string)arr1[1];
            //string dirvalue = String.Empty;
            //string wordvalue = String.Empty;
            //List<decimal> percentl1 = new List<decimal>();
            //List<string> gradel1 = new List<string>();
            //List<decimal> valuel1 = new List<decimal>();

            List<decimal> percentage = new List<decimal>();
            List<string> gradestringlist = new List<string>();
            List<decimal> valuelist = new List<decimal>();
            bool typeerror = false;
            bool change = false;
            

            //this.grades.Sort(this.grades.Columns["Percent"], ListSortDirection.Descending);
            //grades.Columns["Percent"].ValueType = typeof(decimal);

            for (int i = 0; i < grades.RowCount-1; i++)
            {
                //if (grades.Rows[i].Cells["Percent"].FormattedValue == (double)grades.Rows[i].Cells["Percent"].FormattedValue)
                //    percentage.Add(Convert.ToDouble(grades.Rows[i].Cells["Percent"].FormattedValue));
               
                try
                {
                    if ((Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()) <= 100) && (Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()) >= 0))
                    {
                        percentage.Add(Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()));
                    }
                    else
                    {
                        typeerror = true;
                        MessageBox.Show("ERROR!!! Wert in Spalte Percent Reihe " + i + " enthält keine gültige Zahl zwischen 0 & 100!!!", "Keine Zahl zwischen 0 & 100!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch
                {

                    savebtn.DialogResult = DialogResult.None;
                    MessageBox.Show("ERROR!!! Wert in Spalte Percent Reihe " + i + " enthält keine Zahl!!!", "Keine Zahl!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    typeerror = true;
                }
            }

            for (int i = 0; i < grades.RowCount - 1; i++)
            {
                //if (grades.Rows[i].Cells["Percent"].FormattedValue == (double)grades.Rows[i].Cells["Percent"].FormattedValue)
                //    percentage.Add(Convert.ToDouble(grades.Rows[i].Cells["Percent"].FormattedValue));

                if(grades.Rows[i].Cells["Grade"].FormattedValue.ToString() == String.Empty && (((Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()) <= 100) && (Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()) >= 0))))
                {

                    savebtn.DialogResult = DialogResult.None;
                    MessageBox.Show("ERROR!!! Spalte Grade Reihe " + i + " enthält keinen Wert. Es ist leer!!!", "Nichts eingegeben!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    typeerror = true;
                }
                else
                {
                    gradestringlist.Add(grades.Rows[i].Cells["Grade"].FormattedValue.ToString());
                }
            }

            for (int i = 0; i < grades.RowCount - 1; i++)
            {
                //if (grades.Rows[i].Cells["Percent"].FormattedValue == (double)grades.Rows[i].Cells["Percent"].FormattedValue)
                //    percentage.Add(Convert.ToDouble(grades.Rows[i].Cells["Percent"].FormattedValue));
                {
                    try
                    {
                        if ((Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()) <= 100) && (Convert.ToDecimal(grades.Rows[i].Cells["Percent"].FormattedValue.ToString()) >= 0))
                        {
                            valuelist.Add(Convert.ToDecimal(grades.Rows[i].Cells["Value"].FormattedValue.ToString()));
                        }
                    }
                    catch
                    {
                        savebtn.DialogResult = DialogResult.None;
                        MessageBox.Show("ERROR!!! Wert in Spalte Value Reihe " + i + " enthält keine Zahl!!!", "Keine Zahl!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        typeerror = true;
                    }
                }
            }
            try
            {
                Dictionary<decimal, string> dict = new Dictionary<decimal, string>();
                for (int i = 0; i < percentage.Count; i++)
                {
                    if (dict.ContainsKey(percentage[i]) == true)
                    {
                        MessageBox.Show("ERROR!!! Manche Prozentwerte kommen mehrmals vor. Für die korrekte & richtige Berechnung darf jeweils ein Prozentwert nur einmal vorkommen!!!", "Mehrfach vorkommender %Wert!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        typeerror = true;
                    }
                    else
                    {
                        dict.Add(percentage[i], gradestringlist[i]);
                    }
                       
                }

                //throw new Exception();
            }
            catch (ArgumentNullException ane)
            {
                throw new VTException("ArgumentNullException", ane);
            }
            catch (InvalidOperationException inoe)
            {
                throw new VTException("InvalidEnumArgumentException", inoe);
            }
            catch (InvalidEnumArgumentException ieae)
            {
                throw new VTException("InvalidEnumArgumentException", ieae);
            }
            catch (ArgumentException ae)
            {
                throw new VTException("ArgumentException", ae);
            }
            catch (Exception ex)
            {
                throw new VTException("Exception", ex);
            }
            if (typeerror == false)
            {


                List<SingleGrade> sg = new List<SingleGrade>();

                for (int i = 0; i < percentage.Count; i++)
                {
                    sg.Add(new SingleGrade(percentage[i], gradestringlist[i], valuelist[i]));
                }

                try
                {
                    sg.Sort((SingleGrade s1, SingleGrade s2) =>
                    {
                        return s2.percent.CompareTo(s1.percent);
                    });
                }
                catch (ArgumentNullException ane)
                {
                    throw new VTException("ArgumentNullException", ane);
                }
                catch (ArgumentException ae)
                {
                    throw new VTException("ArgumentException", ae);
                }
                catch (Exception ex)
                {
                    throw new VTException("Exception", ex);
                }


                //if (!((grades2.dirvalue == (string)this.direction.SelectedItem) && (grades2.wordvalue == Convert.ToInt32(this.maxwords.Value))) && (grades3.percent != percentage) && (grades3.sentence != gradestringlist) && (grades3.value != valuelist))
                //{
                //    grades2.dirvalue = (string)this.direction.SelectedItem;
                //    grades2.wordvalue = Convert.ToInt32(this.maxwords.Value);
                //    grades2.percent = percentage;
                //    grades2.sentence = gradestringlist;
                //    grades2.value = valuelist;

                //    opt4.txtwrite(grades2);
                //}

                try
                {
                    if ((percentage.Count != grades3.percent.Count) || (gradestringlist.Count != grades3.sentence.Count) || (valuelist.Count != grades3.value.Count) || ((string)grades3.dirvalue != (string)this.direction.SelectedItem) || (grades3.wordvalue != Convert.ToInt32(this.maxwords.Value)))
                    {
                        grades2.dirvalue = (string)this.direction.SelectedItem;
                        grades2.wordvalue = Convert.ToInt32(this.maxwords.Value);

                        for (int i = 0; i < sg.Count; i++)
                        {
                            grades2.percent.Add(sg[i].percent);
                        }
                        for (int i = 0; i < sg.Count; i++)
                        {
                            grades2.sentence.Add(sg[i].sentence);
                        }
                        for (int i = 0; i < sg.Count; i++)
                        {
                            grades2.value.Add(sg[i].value);
                        }

                        opt4.txtwrite(grades2);
                    }
                    else
                    {
                        for (int i = 0; i < percentage.Count; i++)
                        {
                            if (percentage[i] != grades3.percent[i])
                            {
                                change = true;
                            }
                            else
                            {
                                //change = false;
                            }
                        }
                        for (int i = 0; i < gradestringlist.Count; i++)
                        {
                            if (gradestringlist[i] != grades3.sentence[i])
                            {
                                change = true;
                            }
                            else
                            {
                                //change = false;
                            }
                        }
                        for (int i = 0; i < valuelist.Count; i++)
                        {
                            if (valuelist[i] != grades3.value[i])
                            {
                                change = true;
                            }
                            else
                            {
                                //change = false;
                            }
                        }
                    }

                    //throw new ArgumentOutOfRangeException();
                }
                catch (OverflowException ofe)
                {
                    throw new VTException("OverflowException", ofe);
                }
                catch (ArgumentOutOfRangeException aoore)
                {
                    throw new VTException("ArgumentOutOfRangeException", aoore);
                }
                catch (Exception ex)
                {
                    throw new VTException("Exception", ex);
                }

                try
                {
                    if (change == true)
                    {
                        grades2.dirvalue = (string)this.direction.SelectedItem;
                        grades2.wordvalue = Convert.ToInt32(this.maxwords.Value);

                        for (int i = 0; i < sg.Count; i++)
                        {
                            grades2.percent.Add(sg[i].percent);
                        }
                        for (int i = 0; i < sg.Count; i++)
                        {
                            grades2.sentence.Add(sg[i].sentence);
                        }
                        for (int i = 0; i < sg.Count; i++)
                        {
                            grades2.value.Add(sg[i].value);
                        }

                        opt4.txtwrite(grades2);
                    }
                }
                catch (OverflowException ofe)
                {
                    throw new VTException("OverflowException", ofe);
                }
                catch (Exception ex)
                {
                    throw new VTException("Exception", ex);
                }


                //for (int i = 0; i < percentage.Length; i++)
                //{
                //    if()
                //



                try
                {
                    MessageBox.Show("Die Einstellungen wurden gespeichert!", "Gespeichert", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                    //throw new InvalidOperationException();
                }
                catch (InvalidEnumArgumentException ieae)
                {
                    throw new VTException("InvalidEnumArgumentException", ieae);
                }
                catch (InvalidOperationException inoe)
                {
                    throw new VTException("InvalidOperationException", inoe);
                }
                catch (Exception ex)
                {
                    throw new VTException("Exception", ex);
                }
            }
        }

        //private void grades_ColumnHeaderMouseClick(Button savebtn, EventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            //();
            try
            {
                MessageBox.Show("Die Einstellungen wurden nicht gespeichert! Sie haben abgebrochen.", "Nicht Gespeichert", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                //throw new InvalidEnumArgumentException();
            }
            catch (InvalidEnumArgumentException ieae)
            {
                throw new VTException("InvalidEnumArgumentException", ieae);
            }
            catch (InvalidOperationException inoe)
            {
                throw new VTException("InvalidOperationException", inoe);
            }
            catch (Exception ex)
            {
                throw new VTException("Exception", ex);
            }
        }

        //private void grades_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{

        //}

        //private void grades_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //    grades.Sort(grades.Columns["Percent"], ListSortDirection.Descending);
        //}
    }
}
