﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsDialog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dialog my = new Dialog();
           // my.Show(); // nichtmodal

            DialogResult rc = my.ShowDialog(); //modal

            if(rc == DialogResult.Cancel)
            {
                MessageBox.Show("AHA Sie wollen nicht!");
            }

            if(rc == DialogResult.OK)
            {
                MessageBox.Show("JETZT WIRD GESPEICHERT! " + my.txInfo.Text, "OK-Meldung", MessageBoxButtons.YesNo);
            }
        }
    }
}
