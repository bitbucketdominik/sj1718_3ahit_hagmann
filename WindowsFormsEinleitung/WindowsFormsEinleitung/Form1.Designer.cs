﻿namespace WindowsFormsEinleitung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.weiblich = new System.Windows.Forms.RadioButton();
            this.männlich = new System.Windows.Forms.RadioButton();
            this.geschlecht = new System.Windows.Forms.GroupBox();
            this.dienst = new System.Windows.Forms.CheckBox();
            this.teilnahme = new System.Windows.Forms.CheckBox();
            this.geburtsjahr = new System.Windows.Forms.ListBox();
            this.vorname = new System.Windows.Forms.TextBox();
            this.nachname = new System.Windows.Forms.TextBox();
            this.anschrift = new System.Windows.Forms.TextBox();
            this.plz = new System.Windows.Forms.TextBox();
            this.ort = new System.Windows.Forms.TextBox();
            this.senden = new System.Windows.Forms.Button();
            this.geschlecht.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(82, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Teilnahme zum Projekt \"Regionale Innovationen\"";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Vorname:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nachname:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 97);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Anschrift:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "PLZ:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(163, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ort:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Geburtsjahr:";
            // 
            // weiblich
            // 
            this.weiblich.AutoSize = true;
            this.weiblich.Location = new System.Drawing.Point(16, 28);
            this.weiblich.Name = "weiblich";
            this.weiblich.Size = new System.Drawing.Size(63, 17);
            this.weiblich.TabIndex = 7;
            this.weiblich.TabStop = true;
            this.weiblich.Text = "weiblich";
            this.weiblich.UseVisualStyleBackColor = true;
            // 
            // männlich
            // 
            this.männlich.AutoSize = true;
            this.männlich.Location = new System.Drawing.Point(16, 51);
            this.männlich.Name = "männlich";
            this.männlich.Size = new System.Drawing.Size(67, 17);
            this.männlich.TabIndex = 8;
            this.männlich.TabStop = true;
            this.männlich.Text = "männlich";
            this.männlich.UseVisualStyleBackColor = true;
            // 
            // geschlecht
            // 
            this.geschlecht.Controls.Add(this.weiblich);
            this.geschlecht.Controls.Add(this.männlich);
            this.geschlecht.Location = new System.Drawing.Point(236, 168);
            this.geschlecht.Name = "geschlecht";
            this.geschlecht.Size = new System.Drawing.Size(270, 88);
            this.geschlecht.TabIndex = 9;
            this.geschlecht.TabStop = false;
            this.geschlecht.Text = "Geschlecht";
            // 
            // dienst
            // 
            this.dienst.AutoSize = true;
            this.dienst.Location = new System.Drawing.Point(85, 254);
            this.dienst.Name = "dienst";
            this.dienst.Size = new System.Drawing.Size(134, 17);
            this.dienst.TabIndex = 10;
            this.dienst.Text = "Präsenzdienst geleistet";
            this.dienst.UseVisualStyleBackColor = true;
            // 
            // teilnahme
            // 
            this.teilnahme.AutoSize = true;
            this.teilnahme.Location = new System.Drawing.Point(85, 277);
            this.teilnahme.Name = "teilnahme";
            this.teilnahme.Size = new System.Drawing.Size(224, 17);
            this.teilnahme.TabIndex = 11;
            this.teilnahme.Text = "Ich akzeptiere die Teilnahmebedingungen";
            this.teilnahme.UseVisualStyleBackColor = true;
            // 
            // geburtsjahr
            // 
            this.geburtsjahr.FormattingEnabled = true;
            this.geburtsjahr.Location = new System.Drawing.Point(91, 168);
            this.geburtsjahr.Name = "geburtsjahr";
            this.geburtsjahr.Size = new System.Drawing.Size(76, 69);
            this.geburtsjahr.TabIndex = 12;
            // 
            // vorname
            // 
            this.vorname.Location = new System.Drawing.Point(108, 40);
            this.vorname.Name = "vorname";
            this.vorname.Size = new System.Drawing.Size(100, 20);
            this.vorname.TabIndex = 13;
            // 
            // nachname
            // 
            this.nachname.Location = new System.Drawing.Point(108, 66);
            this.nachname.Name = "nachname";
            this.nachname.Size = new System.Drawing.Size(100, 20);
            this.nachname.TabIndex = 14;
            // 
            // anschrift
            // 
            this.anschrift.Location = new System.Drawing.Point(108, 97);
            this.anschrift.Name = "anschrift";
            this.anschrift.Size = new System.Drawing.Size(100, 20);
            this.anschrift.TabIndex = 15;
            // 
            // plz
            // 
            this.plz.Location = new System.Drawing.Point(60, 130);
            this.plz.Name = "plz";
            this.plz.Size = new System.Drawing.Size(51, 20);
            this.plz.TabIndex = 16;
            // 
            // ort
            // 
            this.ort.Location = new System.Drawing.Point(218, 133);
            this.ort.Name = "ort";
            this.ort.Size = new System.Drawing.Size(100, 20);
            this.ort.TabIndex = 17;
            // 
            // senden
            // 
            this.senden.Location = new System.Drawing.Point(184, 322);
            this.senden.Name = "senden";
            this.senden.Size = new System.Drawing.Size(199, 23);
            this.senden.TabIndex = 18;
            this.senden.Text = "Senden";
            this.senden.UseVisualStyleBackColor = true;
            this.senden.Click += new System.EventHandler(this.senden_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 383);
            this.Controls.Add(this.senden);
            this.Controls.Add(this.ort);
            this.Controls.Add(this.plz);
            this.Controls.Add(this.anschrift);
            this.Controls.Add(this.nachname);
            this.Controls.Add(this.vorname);
            this.Controls.Add(this.geburtsjahr);
            this.Controls.Add(this.teilnahme);
            this.Controls.Add(this.dienst);
            this.Controls.Add(this.geschlecht);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.geschlecht.ResumeLayout(false);
            this.geschlecht.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton weiblich;
        private System.Windows.Forms.RadioButton männlich;
        private System.Windows.Forms.GroupBox geschlecht;
        private System.Windows.Forms.CheckBox dienst;
        private System.Windows.Forms.CheckBox teilnahme;
        private System.Windows.Forms.ListBox geburtsjahr;
        private System.Windows.Forms.TextBox vorname;
        private System.Windows.Forms.TextBox nachname;
        private System.Windows.Forms.TextBox anschrift;
        private System.Windows.Forms.TextBox plz;
        private System.Windows.Forms.TextBox ort;
        private System.Windows.Forms.Button senden;
    }
}

