﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsEinleitung
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            for (int i = 1898; i < 2018; i++)
            {
                geburtsjahr.Items.Add(i);
            }
        }

        private void senden_Click(object sender, EventArgs e)
        {
            string geschlecht;
            if(weiblich.Checked==true)
            {
                geschlecht = weiblich.Text;
            }
            else
            {
                geschlecht = männlich.Text;
            }

            string diensttext;
            if(dienst.Checked)
            {
                diensttext = "Präsenzdienst wurde geleistet!";
            }
            else
            {
                diensttext = "Präsenzdienst wurde nicht geleistet!";
            }

            string teilnahmetext;
            if (teilnahme.Checked)
            {
                teilnahmetext = "Teilnahmebedingungen akzeptiert!";
            }
            else
            {
                teilnahmetext = "Teilnahmebedingungen nicht akzeptiert!";
            }

            string resulttext = "Sie haben sich mit folgenden Daten bei uns angemeldet: \n \n" + vorname.Text + " " + nachname.Text + "\n" + anschrift.Text + ", " + plz.Text + " " + ort.Text + "\n" + Convert.ToString(geburtsjahr.SelectedItem) + ", " + geschlecht + "\n" + diensttext + "\n" + teilnahmetext;
            MessageBox.Show(resulttext);
        }
    }
}
