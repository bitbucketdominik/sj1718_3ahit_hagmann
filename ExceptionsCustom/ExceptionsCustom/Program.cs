﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionsCustom
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int> { 2, 1, 20, 25, 921, 55, 44 };
            List<int> emptylist = null;

            Console.WriteLine(average(list));
            Console.WriteLine("         ");
            Console.WriteLine(average(emptylist));

            Console.ReadLine();
        }

        static double average (List<int> list)
        {
            double sum = 0;

            try
            {

                for (int i = 0; i < list.Count; i++)
                {
                    sum += list[i];
                }

                double average = sum / list.Count;

                return average;

            }
            catch(Exception e)
            {

                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Exception\n ||| TargetSite: " + e.TargetSite + "\n ||| StackTrace: " + e.StackTrace + "\n ||| Source: " + e.Source + "\n ||| InnerException: " + e.InnerException + "\n ||| Message: " +  e.Message);
                
            }

            return 0;
        }
    }
}
