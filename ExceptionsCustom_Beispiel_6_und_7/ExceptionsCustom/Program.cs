﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionsCustom
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> list = new List<int> { 2, 1, 20, 25, 921, 55, 44 };
            List<int> emptylist = new List<int>();

            try
            {
                Console.WriteLine(average(list));
            }
            catch(MyFirstException mfe)
            {
                Console.WriteLine("                                                             ");
                Console.WriteLine("Das Programm konnte leider nicht den Durchschnitt der angegebenen Liste berechnen! " + mfe.Message);
            }

            Console.WriteLine("         ");

            try
            {
                Console.WriteLine(average(emptylist));
            }
            catch (MyFirstException mfe)
            {
                Console.WriteLine("Das Programm konnte leider nicht den Durchschnitt der angegebenen Liste berechnen! " + mfe.Message);
            }

            Console.ReadLine();
        }

        static double average (List<int> list)
        {
            try
            {
                return list.Average();
            }
            catch(Exception e)
            {
                throw new MyFirstException("Problem detected", e);
            }
        }
    }
}
