﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ExceptionsCustom
{
    [Serializable]
    class MyFirstException : ApplicationException
    {
        public MyFirstException()
        {

        }

        public MyFirstException(string message) : base (message)
        {
            Console.WriteLine(DateTime.Now + " " + message);
        }

        public MyFirstException(string message, Exception inner) : base (message, inner)
        {
            Console.WriteLine(DateTime.Now + " " + message + " " + inner.Message);
        }
    }
}
