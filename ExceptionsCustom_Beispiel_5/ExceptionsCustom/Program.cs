﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionsCustom
{
    class Program
    {
        static void Main(string[] args)
        {
            double av = 0;

            List<int> list = new List<int> { 201, 1, 219, 25, 921, 55, 44, 1050, 521, 1153, 210 };
            List<int> emptylist = new List<int>();

            //With tester-doer-pattern

            if(averageTryParse(list, out av) == true)
            {
                Console.WriteLine(list.Average());
            }
            
            Console.WriteLine("         ");

            if (averageTryParse(emptylist, out av) == true)
            {
                Console.WriteLine(emptylist.Average());
            }

            Console.ReadLine();
        }

        static public bool averageTryParse (List<int> l, out double av)
        {
            av = 0;
            try
            {

                av = l.Average();
                

                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
