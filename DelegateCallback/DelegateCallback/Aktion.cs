﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateCallback
{
    class Aktion
    {
        public delegate void Callback(string s);

        public void Greeting(Callback obj)
        {
            DateTime dt = DateTime.Now;

            int hour = dt.Hour;

            if(hour<12)
            {
                obj("Guten Morgen");
            }

            else
            {
                obj("Hallo");
            }
        }
    }
}
