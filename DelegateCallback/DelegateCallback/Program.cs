﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateCallback
{
    class Program
    {
        static void Main(string[] args)
        {
            Aktion akt = new Aktion();
            akt.Greeting(MethFromProgramNick);
            akt.Greeting(MethFromProgramFull);
            Console.ReadLine();
        }

        static void MethFromProgramNick(string s)
        {
            Console.WriteLine(s + " Hans!");
        }

        static void MethFromProgramFull(string s)
        {
            Console.WriteLine(s + " Herr Hans Mayer!");
        }
    }
}
