﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mitfahrzentrale
{
    public class Strecke
    {
        private Mitfahrzentrale.Mitfahrer[] Mitfahrer = new Mitfahrzentrale.Mitfahrer[Sitzplaetze];
        private double Fahrtdauer;

        public Strecke(Fahrer Fahrer, string Start, string Ziel, int Sitzplaetze, DateTime Startzeit, DateTime Ankunftszeit)
        {
            throw new System.NotImplementedException();
        }

        static public Fahrer Fahrer
        {
            get; set;
        }

        public string Ziel
        {
            get; set;
        }

        public string Start
        {
            get; set;
        }

        public DateTime Startzeit
        {
            get; set;
        }

        public DateTime Ankunftszeit
        {
            get; set;
        }

        static public int Sitzplaetze
        {
            get; set;
        }
    }
}