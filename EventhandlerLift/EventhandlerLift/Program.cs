﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventhandlerLift
{
    class Program
    {
        static void Main(string[] args)
        {
            Lift lift1 = new Lift { Bezeichnung = "2009/12345", Max = 2000 };

            lift1.overload += Alarm;

            Person p1 = new Person { Name = "Otto Uwe Berger", Gewicht = 1100 };
            Person p2 = new Person { Name = "Armin Müller", Gewicht = 800 };
            Person p3 = new Person { Name = "Vinzenz Neumann", Gewicht = 500 };

            lift1.Zusteigen(p1);
            lift1.Zusteigen(p2);
            lift1.Zusteigen(p3);

            //Console.WriteLine(lift1);

            Console.ReadLine();
        }

        public static void Alarm(Lift lalarm, Person palarm)
        {
            Console.WriteLine("ACHTUNG bei Lift " + lalarm.Bezeichnung + ", " + palarm.Name.ToString() + " bitte aus dem Lift aussteigen, sonst kann der Lift nicht abfahren! \n");
        }
    }
}
