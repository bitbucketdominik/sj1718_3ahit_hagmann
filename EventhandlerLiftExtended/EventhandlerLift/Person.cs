﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventhandlerLift
{
    class Person
    {
        public int Gewicht { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name + " " + Gewicht;
        }
    }
}
