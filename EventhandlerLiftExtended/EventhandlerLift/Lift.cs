﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventhandlerLift
{
    class Lift
    {
        public delegate void OverloadLift(Lift l, Person p);
        public event OverloadLift overload;

        public double Max { get; set; }
        public string Bezeichnung { get; set; }

        List<Person> pliste = new List<Person>();

        double gewichtinsgesamt = 0;

        public override string ToString()
        {
            string lstring = "Aktuelle Personen im Lift " + Bezeichnung + ": \n";
            foreach (Person phelp in pliste)
            {
                lstring += "\n" + phelp + "\n";
            }

            return lstring;
        }

        public void Zusteigen (Person p)
        {
            if(gewichtinsgesamt + p.Gewicht <= this.Max)
            {
                pliste.Add(p);
                gewichtinsgesamt += p.Gewicht;
            }
            else
            {
                pliste.Add(p);
                gewichtinsgesamt += p.Gewicht;
                pliste.Sort((Person p1, Person p2) => { return p1.Gewicht.CompareTo(p2.Gewicht); });

                for (int i = 0; i < pliste.Count; i++)
                {
                    if (gewichtinsgesamt - pliste[i].Gewicht <= this.Max)
                    {
                        overload(this, pliste[i]);
                        break;
                    }
                }

                

            }
        }
    }
}
