﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VT_WPF_DLL;
using System.Data.OleDb;

namespace VT_WPF
{
    class Program
    {
        static void Main(string[] args)
        {
            //myTests();

            //dbCheck();

            //ins2Result();

            Vocabulary client;

            if (args[0] == "fixed")
            {
                client = new Vocabulary(new Service());
                client.Start();
            }

            if (args[0] == "file")
            {
                client = new Vocabulary(new WordsFile());
                client.Start();
            }

            if (args[0] == "database")
            {
                client = new Vocabulary(new WordsDatabase());
                client.Start();
            }

            else
            {
                client = new Vocabulary(new Service());
                client.Start();
            }

            VTConnection.Closer();

            Console.ReadLine();
        }

        //static void myTests()
        //{
        //    Vocabulary vfixed = new Vocabulary();
        //    Vocabulary vfile = new Vocabulary();
        //    //Vocabulary vdatabase = new Vocabulary("database");
        //    Vocabulary vrandom = new Vocabulary();
        //    List<Word> lwrandom = vrandom.RandomList();

        //    foreach (Word w in vfixed.WordList)
        //    {
        //        Console.WriteLine(w.ToString());
        //    }

        //    Console.WriteLine("                         ");

        //    foreach (Word w in vfile.WordList)
        //    {
        //        Console.WriteLine(w.ToString());
        //    }

        //    Console.WriteLine("                         ");

        //    foreach (Word w in lwrandom)
        //    {
        //        Console.WriteLine(w.ToString());
        //    }

        //    bool check = vfile.Check("siebenunddreißig", "thirty-seven");

        //    bool checkvoc = vfile.Check("Holz", "wood");

        //    Console.WriteLine("                         ");

        //    Console.WriteLine(checkvoc);

        //    Console.WriteLine("                         ");

        //    Console.WriteLine(check);
        //}

        //static void dbCheck()
        //{
        //    Vocabulary vdatabase = new Vocabulary();

        //    Console.WriteLine("                         ");
        //    Console.WriteLine("                         ");
        //    Console.WriteLine("Words from the database: ");
        //    Console.WriteLine("English German");
        //    Console.WriteLine("                         ");

        //    foreach (Word w in vdatabase.WordList)
        //    {
        //        Console.WriteLine(w.ToString());
        //        Console.WriteLine("                         ");
        //    }

        //    Console.WriteLine("                         ");
        //}

        //static OleDbConnection con = null;

        //static void ins2Result()
        //{
        //    //string conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=VocTrainer.mdb";
        //    OleDbConnection con = VTConnection.Loader();
        //    OleDbCommand cmd = new OleDbCommand();
        //    cmd.Connection = con;
        //    DateTime date = DateTime.Now;
        //    double ticks = date.ToOADate();

        //    Console.WriteLine("                         ");
        //    Console.WriteLine("Insert some rows into results");
        //    Console.WriteLine("                         ");
        //    for (int i = 22; i <= 37; i++)
        //    {
        //        cmd.CommandText = "Insert into results (Positive, Negative, ExDate) values (" + (5+i) + ", " + (2+i) + ", " + ticks + ")";
                
        //        Console.WriteLine(cmd.ExecuteNonQuery());
        //    }
        //}
    }
}
