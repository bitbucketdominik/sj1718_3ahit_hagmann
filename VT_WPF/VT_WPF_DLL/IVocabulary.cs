﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VT_WPF_DLL
{
    public interface IVocabulary
    {
        List<Word> Serve();
    }
}
