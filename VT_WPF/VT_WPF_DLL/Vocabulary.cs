﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace VT_WPF_DLL
{
    public class Vocabulary
    {
        public List<Word> WordList;

        public bool Check(string original, string translated) //Gibt true zurück, wenn das Wort korrekt übersetzt wurde, gibt false zurück, wenn das Wort inkorrekt übersetzt wurde
        {
            Word w = new Word(original, translated);
            bool b = false;
            foreach (Word wf in WordList)
            {
                if (w.ToString() == wf.ToString())
                {
                    b = true;
                    break;
                }
                else
                {
                    b = false;
                }
            }
            return b;
        }

        public List<Word> RandomList()
        {
            LoadListFixed();

            List<Word> randomsortedlist = new List<Word>();
            Random generator = new Random();

            while (WordList.Count > 0)
            {
                int position = generator.Next(WordList.Count);
                randomsortedlist.Add(WordList[position]);
                WordList.RemoveAt(position);
            }

            return randomsortedlist;

        }

        public void LoadListFixed()
        {
            List<Word> lw = new List<Word>
            {
                new Word("grün", "green"),
                new Word("blau", "blue"),
                new Word("rot", "red"),
                new Word("gelb", "yellow"),
                new Word("schwarz", "black"),
                new Word("weiß", "white"),
                new Word("grau", "grey"),
                new Word("orange", "orange"),
                new Word("Drucker", "printer"),
                new Word("Fenster", "window"),
                new Word("Betriebssystem", "operating system"),
                new Word("Taschenrechner", "calculator"),
                new Word("Helligkeit", "brightness"),
                new Word("Farbe", "color"),
                new Word("eins", "one"),
                new Word("zwei", "two"),
                new Word("drei", "three"),
                new Word("vier", "four"),
                new Word("fünf", "five"),
                new Word("sechs", "six"),
                new Word("sieben", "seven"),
                new Word("acht", "eight"),
                new Word("neun", "nine"),
                new Word("zehn", "ten"),
                new Word("elf", "eleven")
            };

            WordList = lw;
        }

        public void LoadListFile()
        {
            StreamReader sr = null;
            try
            {
                sr = new StreamReader(@"G:\SEW\VT_WPF\VT_WPF_DLL\bin\Debug\words.voc");

                List<Word> lw = new List<Word>();

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] o = s.Split('!');
                    lw.Add(new Word(o[0], o[1]));
                }

                WordList = lw;
            }
            catch (FileNotFoundException fnfe)
            {
                LoadListFixed();
            }
            catch (Exception e)
            {
                LoadListFixed();
            }
        }

        public void LoadListDatabase()
        {
            List<Word> wordl = new List<Word>();
            //string conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=VocTrainer.mdb";
            OleDbConnection con = VTConnection.Loader();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select English, German from words";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string english = "";
                if (reader.GetValue(0) != System.DBNull.Value)
                {
                    english = reader.GetString(0);
                }
                string german = "";
                if (reader.GetValue(1) != System.DBNull.Value)
                {
                    german = reader.GetString(1);
                }
                wordl.Add(new Word(english, german));
            }

            WordList = wordl;
        }

        //public Vocabulary(string b)
        //{
        //    if(b == "fixed")
        //    {
        //        LoadListFixed();
        //    }
        //    if(b == "file")
        //    {
        //        LoadListFile();
        //    }
        //    if (b == "database")
        //    {
        //        LoadListDatabase();
        //    }
        //    else
        //    {
        //        return;
        //    }
        //}
        IVocabulary _service;
        public Vocabulary(IVocabulary service)
        {
            this._service = service;
        }

        public void Start()
        {
            //Console.WriteLine("Service Started");
            List<Word> lw1 = this._service.Serve();
            for (int i = 0; i < lw1.Count; i++)
            {
                Console.WriteLine(lw1[i].ToString());
            }
        }
    }
}