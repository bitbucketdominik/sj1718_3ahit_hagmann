﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace VT_WPF_DLL
{
    public class WordsFile : IVocabulary
    {
        public List<Word> Serve()
        {
            StreamReader sr = null;
                sr = new StreamReader(@"G:\SEW\VT_WPF\VT_WPF_DLL\bin\Debug\words.voc");

                List<Word> lw = new List<Word>();

                while (!sr.EndOfStream)
                {
                    string s = sr.ReadLine();
                    string[] o = s.Split('!');
                    lw.Add(new Word(o[0], o[1]));
                }

                return lw;
        }
    }
}
