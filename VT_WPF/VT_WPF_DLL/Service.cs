﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VT_WPF_DLL
{
    public class Service : IVocabulary
    {
        public List<Word> Serve()
        {
            List<Word> lw = new List<Word>
            {
                new Word("grün", "green"),
                new Word("blau", "blue"),
                new Word("rot", "red"),
                new Word("gelb", "yellow"),
                new Word("schwarz", "black"),
                new Word("weiß", "white"),
                new Word("grau", "grey"),
                new Word("orange", "orange"),
                new Word("Drucker", "printer"),
                new Word("Fenster", "window"),
                new Word("Betriebssystem", "operating system"),
                new Word("Taschenrechner", "calculator"),
                new Word("Helligkeit", "brightness"),
                new Word("Farbe", "color"),
                new Word("eins", "one"),
                new Word("zwei", "two"),
                new Word("drei", "three"),
                new Word("vier", "four"),
                new Word("fünf", "five"),
                new Word("sechs", "six"),
                new Word("sieben", "seven"),
                new Word("acht", "eight"),
                new Word("neun", "nine"),
                new Word("zehn", "ten"),
                new Word("elf", "eleven")
            };
            return lw;
        }
    }
}
