﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace VT_WPF_DLL
{
    public class VTConnection
    {
        static private OleDbConnection olevtc;

        private VTConnection()
        {
            string conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=VocTrainer.mdb";
            olevtc = new OleDbConnection(conStr);
            olevtc.Open();
        }

        public static OleDbConnection Loader()
        {
            if(olevtc == null)
            {
                VTConnection vtc__ = new VTConnection();
            }

            return olevtc;
        }

        public static void Closer()
        {
            if (olevtc != null)
            {
                olevtc.Close();
            }

            olevtc = null;
        }
    }
}
