﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace VT_WPF_DLL
{
    public class WordsDatabase : IVocabulary
    {
        public List<Word> Serve()
        {
            List<Word> wordl = new List<Word>();
            //string conStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=VocTrainer.mdb";
            OleDbConnection con = VTConnection.Loader();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "Select English, German from words";
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string english = "";
                if (reader.GetValue(0) != System.DBNull.Value)
                {
                    english = reader.GetString(0);
                }
                string german = "";
                if (reader.GetValue(1) != System.DBNull.Value)
                {
                    german = reader.GetString(1);
                }
                wordl.Add(new Word(english, german));
            }

            return wordl;
        }
    }
}
