﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VT_WPF_DLL
{
    public class Word
    {
        private string original;
        private string translated;

        public Word (string original, string translated)
        {
            this.original = original;
            this.translated = translated;
        }

        public override string ToString()
        {
            return original + " " + translated;
        }
    }
}
