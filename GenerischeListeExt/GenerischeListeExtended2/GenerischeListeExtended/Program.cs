﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerischeListeExtended
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Firma> f = new List<Firma>
            {
                new Firma{Plz=2620, Ort="Neunkirchen", Name="BASF Chemicals", Mitarbeiteranzahl=1503, Umsatz=12502005.51, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Niklas Schroder", Personalnummer=724415 }, new Mitarbeiter() { Name = "Uwe Fink", Personalnummer = 250597 }, new Mitarbeiter() { Name = "Florian Lange", Personalnummer = 214495 }, new Mitarbeiter() { Name = "Sabrina Kaiser", Personalnummer = 839439 }, new Mitarbeiter() { Name = "Peter Schröder", Personalnummer = 809031 }, new Mitarbeiter() { Name = "Marie Meier", Personalnummer = 552148 }, new Mitarbeiter() { Name = "Katharina Finkel", Personalnummer = 303581 }, new Mitarbeiter() { Name = "Ralph Krüger", Personalnummer = 541461 }, new Mitarbeiter() { Name = "Ulrike Nacht", Personalnummer = 390255 }, new Mitarbeiter() { Name = "Christian Wechsler", Personalnummer = 392697 }, } },
                new Firma{Plz=3910, Ort="Zwettl", Name="Kastner GmbH", Mitarbeiteranzahl=980, Umsatz=5602502.63, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Frank Kohler", Personalnummer= 701927 }, new Mitarbeiter() { Name = "Doreen Winkel", Personalnummer = 572297 }, new Mitarbeiter() { Name = "Steffen Lang", Personalnummer = 612919 }, new Mitarbeiter() { Name = "Max Schradin", Personalnummer = 422706 }, new Mitarbeiter() { Name = "Anna Heesch", Personalnummer = 421759 }, new Mitarbeiter() { Name = "Tina Kaiser", Personalnummer = 937024 }, new Mitarbeiter() { Name = "Sophia Vogel", Personalnummer = 690567 }, new Mitarbeiter() { Name = "Sarah Schuster", Personalnummer = 579211 }, new Mitarbeiter() { Name = "Maximillian König", Personalnummer = 400532 }, new Mitarbeiter() { Name = "Markus Meier", Personalnummer = 553076 }, } },
                new Firma{Plz=3943, Ort="Schrems", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=1143, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Lena Weißmüller", Personalnummer= 106377 }, new Mitarbeiter() { Name = "Kerstin Eberhardt", Personalnummer = 888195 }, new Mitarbeiter() { Name = "Tanja Schultz", Personalnummer = 754852 }, new Mitarbeiter() { Name = "Jörg Metzger", Personalnummer = 760720 }, new Mitarbeiter() { Name = "Thomas Schürmann", Personalnummer = 880910 }, new Mitarbeiter() { Name = "Jürgen Milski", Personalnummer = 938344 }, new Mitarbeiter() { Name = "Matthias Brandt", Personalnummer = 109448 }, new Mitarbeiter() { Name = "Jessica Ziegler", Personalnummer = 937530 }, new Mitarbeiter() { Name = "Robert Achen", Personalnummer = 521639 }, new Mitarbeiter() { Name = "Swen Richter", Personalnummer = 577936 }, } },
                new Firma{Plz=6460, Ort="Imst", Name="Zirbenholztischlerei Riml", Mitarbeiteranzahl=12, Umsatz=50025.55, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Tim Friedmann", Personalnummer= 503341 }, new Mitarbeiter() { Name = "Franziska Gerbert", Personalnummer = 299716 }, new Mitarbeiter() { Name = "Marie Hahn", Personalnummer = 278794 }, new Mitarbeiter() { Name = "Juliane Wernher", Personalnummer = 782524 }, new Mitarbeiter() { Name = "Kathrin Sankt", Personalnummer = 589204 }, new Mitarbeiter() { Name = "Uta Wolf", Personalnummer = 205980 }, new Mitarbeiter() { Name = "Michael Böhminger", Personalnummer = 523707 }, new Mitarbeiter() { Name = "Bernd Kaufmann", Personalnummer = 166427 }, new Mitarbeiter() { Name = "Frank Zimmer", Personalnummer = 804801 }, new Mitarbeiter() { Name = "Daniela Sonntag", Personalnummer = 586786 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=555, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Nicole Böhm", Personalnummer= 829483 }, new Mitarbeiter() { Name = "Franziska Gerber", Personalnummer = 993372 }, new Mitarbeiter() { Name = "Jan Amsel", Personalnummer = 573213 }, new Mitarbeiter() { Name = "Marco Schmidt", Personalnummer = 995896 }, new Mitarbeiter() { Name = "Manuela Winkel", Personalnummer = 619780 }, new Mitarbeiter() { Name = "Maik Berg", Personalnummer = 728071 }, new Mitarbeiter() { Name = "Antje Neumann", Personalnummer = 155479 }, new Mitarbeiter() { Name = "Stephanie Peters", Personalnummer = 221289 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 575835 }, new Mitarbeiter() { Name = "Maik Hübner", Personalnummer = 437003 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Pizzeria San Marco", Mitarbeiteranzahl=3, Umsatz=78526.02, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Simone Jäger", Personalnummer= 977047 }, new Mitarbeiter() { Name = "Jürgen Müller", Personalnummer = 554905 }, new Mitarbeiter() { Name = "Annett Orner", Personalnummer = 296987 }, new Mitarbeiter() { Name = "Anja Nussbaum", Personalnummer = 558412 }, new Mitarbeiter() { Name = "Tanja Seiler", Personalnummer = 272545 }, new Mitarbeiter() { Name = "Maximillian Baumgartner", Personalnummer = 395430 }, new Mitarbeiter() { Name = "Stephanie Eisenberg", Personalnummer = 140201 }, new Mitarbeiter() { Name = "Barbara Decker", Personalnummer = 613096 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 864056 }, new Mitarbeiter() { Name = "Lukas Jäger", Personalnummer = 798737 }, } },
                new Firma{Plz=9800, Ort="Spittal an der Drau", Name="Mineralöle Leitner", Mitarbeiteranzahl=6, Umsatz=96325.12, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Maik Ehrlichmann", Personalnummer= 327798 }, new Mitarbeiter() { Name = "Leon Schuster", Personalnummer = 735626 }, new Mitarbeiter() { Name = "Torsten Traugott", Personalnummer = 687167 }, new Mitarbeiter() { Name = "Martina Bayer", Personalnummer = 259549 }, new Mitarbeiter() { Name = "Katja Münch", Personalnummer = 470189 }, new Mitarbeiter() { Name = "Dirk Altmann", Personalnummer = 734471 }, new Mitarbeiter() { Name = "Ulrich Lehrer", Personalnummer = 959922 }, new Mitarbeiter() { Name = "Bernd Eiffel", Personalnummer = 275658 }, new Mitarbeiter() { Name = "Matthias Brauer", Personalnummer = 986830 }, new Mitarbeiter() { Name = "Manuela Fischer", Personalnummer = 434674 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Fahrschule Pannonia", Mitarbeiteranzahl=22, Umsatz=101893.98, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Sarah Hermann", Personalnummer= 929309 }, new Mitarbeiter() { Name = "Martin Lang", Personalnummer = 509057 }, new Mitarbeiter() { Name = "Anke Frühauf", Personalnummer = 845379 }, new Mitarbeiter() { Name = "Lucas Langmann", Personalnummer = 464069 }, new Mitarbeiter() { Name = "Dieter Kohler", Personalnummer = 209170 }, new Mitarbeiter() { Name = "Juliane Blau", Personalnummer = 399676 }, new Mitarbeiter() { Name = "Andrea Gruber", Personalnummer = 320523 }, new Mitarbeiter() { Name = "Benjamin Sommer", Personalnummer = 485817 }, new Mitarbeiter() { Name = "Ernestine Bleich", Personalnummer = 555064 }, new Mitarbeiter() { Name = "Jens Fenstermacher", Personalnummer = 495463 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=541, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Michelle Kuhn", Personalnummer= 223838 }, new Mitarbeiter() { Name = "Niklas Goldschmidt", Personalnummer = 797250 }, new Mitarbeiter() { Name = "Stephanie Bergmann", Personalnummer = 655021 }, new Mitarbeiter() { Name = "Silke Zimmer", Personalnummer = 437785 }, new Mitarbeiter() { Name = "Antje Hoch", Personalnummer = 414790 }, new Mitarbeiter() { Name = "Selina Grünewald", Personalnummer = 554280 }, new Mitarbeiter() { Name = "Jennifer Eggers", Personalnummer = 776081 }, new Mitarbeiter() { Name = "Kerstin Hertz", Personalnummer = 287285 }, new Mitarbeiter() { Name = "Torsten Lehmann", Personalnummer = 163138 }, new Mitarbeiter() { Name = "Sophie Hertzog", Personalnummer = 437856 }, } }
            };

            List<Firma> fnew = new List<Firma>
            {
                new Firma{Plz=2620, Ort="Neunkirchen", Name="BASF Chemicals", Mitarbeiteranzahl=1503, Umsatz=12502005.51, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Niklas Schroder", Personalnummer=724415 }, new Mitarbeiter() { Name = "Uwe Fink", Personalnummer = 250597 }, new Mitarbeiter() { Name = "Florian Lange", Personalnummer = 214495 }, new Mitarbeiter() { Name = "Sabrina Kaiser", Personalnummer = 839439 }, new Mitarbeiter() { Name = "Peter Schröder", Personalnummer = 809031 }, new Mitarbeiter() { Name = "Marie Meier", Personalnummer = 552148 }, new Mitarbeiter() { Name = "Katharina Finkel", Personalnummer = 303581 }, new Mitarbeiter() { Name = "Ralph Krüger", Personalnummer = 541461 }, new Mitarbeiter() { Name = "Ulrike Nacht", Personalnummer = 390255 }, new Mitarbeiter() { Name = "Christian Wechsler", Personalnummer = 392697 }, } },
                new Firma{Plz=3910, Ort="Zwettl", Name="Kastner GmbH", Mitarbeiteranzahl=980, Umsatz=5602502.63, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Frank Kohler", Personalnummer= 701927 }, new Mitarbeiter() { Name = "Doreen Winkel", Personalnummer = 572297 }, new Mitarbeiter() { Name = "Steffen Lang", Personalnummer = 612919 }, new Mitarbeiter() { Name = "Max Schradin", Personalnummer = 422706 }, new Mitarbeiter() { Name = "Anna Heesch", Personalnummer = 421759 }, new Mitarbeiter() { Name = "Tina Kaiser", Personalnummer = 937024 }, new Mitarbeiter() { Name = "Sophia Vogel", Personalnummer = 690567 }, new Mitarbeiter() { Name = "Sarah Schuster", Personalnummer = 579211 }, new Mitarbeiter() { Name = "Maximillian König", Personalnummer = 400532 }, new Mitarbeiter() { Name = "Markus Meier", Personalnummer = 553076 }, } },
                new Firma{Plz=3943, Ort="Schrems", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=1143, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Lena Weißmüller", Personalnummer= 106377 }, new Mitarbeiter() { Name = "Kerstin Eberhardt", Personalnummer = 888195 }, new Mitarbeiter() { Name = "Tanja Schultz", Personalnummer = 754852 }, new Mitarbeiter() { Name = "Jörg Metzger", Personalnummer = 760720 }, new Mitarbeiter() { Name = "Thomas Schürmann", Personalnummer = 880910 }, new Mitarbeiter() { Name = "Jürgen Milski", Personalnummer = 938344 }, new Mitarbeiter() { Name = "Matthias Brandt", Personalnummer = 109448 }, new Mitarbeiter() { Name = "Jessica Ziegler", Personalnummer = 937530 }, new Mitarbeiter() { Name = "Robert Achen", Personalnummer = 521639 }, new Mitarbeiter() { Name = "Swen Richter", Personalnummer = 577936 }, } },
                new Firma{Plz=6460, Ort="Imst", Name="Zirbenholztischlerei Riml", Mitarbeiteranzahl=12, Umsatz=50025.55, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Tim Friedmann", Personalnummer= 503341 }, new Mitarbeiter() { Name = "Franziska Gerbert", Personalnummer = 299716 }, new Mitarbeiter() { Name = "Marie Hahn", Personalnummer = 278794 }, new Mitarbeiter() { Name = "Juliane Wernher", Personalnummer = 782524 }, new Mitarbeiter() { Name = "Kathrin Sankt", Personalnummer = 589204 }, new Mitarbeiter() { Name = "Uta Wolf", Personalnummer = 205980 }, new Mitarbeiter() { Name = "Michael Böhminger", Personalnummer = 523707 }, new Mitarbeiter() { Name = "Bernd Kaufmann", Personalnummer = 166427 }, new Mitarbeiter() { Name = "Frank Zimmer", Personalnummer = 804801 }, new Mitarbeiter() { Name = "Daniela Sonntag", Personalnummer = 586786 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=555, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Nicole Böhm", Personalnummer= 829483 }, new Mitarbeiter() { Name = "Franziska Gerber", Personalnummer = 993372 }, new Mitarbeiter() { Name = "Jan Amsel", Personalnummer = 573213 }, new Mitarbeiter() { Name = "Marco Schmidt", Personalnummer = 995896 }, new Mitarbeiter() { Name = "Manuela Winkel", Personalnummer = 619780 }, new Mitarbeiter() { Name = "Maik Berg", Personalnummer = 728071 }, new Mitarbeiter() { Name = "Antje Neumann", Personalnummer = 155479 }, new Mitarbeiter() { Name = "Stephanie Peters", Personalnummer = 221289 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 575835 }, new Mitarbeiter() { Name = "Maik Hübner", Personalnummer = 437003 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Pizzeria San Marco", Mitarbeiteranzahl=3, Umsatz=78526.02, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Simone Jäger", Personalnummer= 977047 }, new Mitarbeiter() { Name = "Jürgen Müller", Personalnummer = 554905 }, new Mitarbeiter() { Name = "Annett Orner", Personalnummer = 296987 }, new Mitarbeiter() { Name = "Anja Nussbaum", Personalnummer = 558412 }, new Mitarbeiter() { Name = "Tanja Seiler", Personalnummer = 272545 }, new Mitarbeiter() { Name = "Maximillian Baumgartner", Personalnummer = 395430 }, new Mitarbeiter() { Name = "Stephanie Eisenberg", Personalnummer = 140201 }, new Mitarbeiter() { Name = "Barbara Decker", Personalnummer = 613096 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 864056 }, new Mitarbeiter() { Name = "Lukas Jäger", Personalnummer = 798737 }, } },
                new Firma{Plz=9800, Ort="Spittal an der Drau", Name="Mineralöle Leitner", Mitarbeiteranzahl=6, Umsatz=96325.12, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Maik Ehrlichmann", Personalnummer= 327798 }, new Mitarbeiter() { Name = "Leon Schuster", Personalnummer = 735626 }, new Mitarbeiter() { Name = "Torsten Traugott", Personalnummer = 687167 }, new Mitarbeiter() { Name = "Martina Bayer", Personalnummer = 259549 }, new Mitarbeiter() { Name = "Katja Münch", Personalnummer = 470189 }, new Mitarbeiter() { Name = "Dirk Altmann", Personalnummer = 734471 }, new Mitarbeiter() { Name = "Ulrich Lehrer", Personalnummer = 959922 }, new Mitarbeiter() { Name = "Bernd Eiffel", Personalnummer = 275658 }, new Mitarbeiter() { Name = "Matthias Brauer", Personalnummer = 986830 }, new Mitarbeiter() { Name = "Manuela Fischer", Personalnummer = 434674 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Fahrschule Pannonia", Mitarbeiteranzahl=22, Umsatz=101893.98, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Sarah Hermann", Personalnummer= 929309 }, new Mitarbeiter() { Name = "Martin Lang", Personalnummer = 509057 }, new Mitarbeiter() { Name = "Anke Frühauf", Personalnummer = 845379 }, new Mitarbeiter() { Name = "Lucas Langmann", Personalnummer = 464069 }, new Mitarbeiter() { Name = "Dieter Kohler", Personalnummer = 209170 }, new Mitarbeiter() { Name = "Juliane Blau", Personalnummer = 399676 }, new Mitarbeiter() { Name = "Andrea Gruber", Personalnummer = 320523 }, new Mitarbeiter() { Name = "Benjamin Sommer", Personalnummer = 485817 }, new Mitarbeiter() { Name = "Ernestine Bleich", Personalnummer = 555064 }, new Mitarbeiter() { Name = "Jens Fenstermacher", Personalnummer = 495463 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=541, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Michelle Kuhn", Personalnummer= 223838 }, new Mitarbeiter() { Name = "Niklas Goldschmidt", Personalnummer = 797250 }, new Mitarbeiter() { Name = "Stephanie Bergmann", Personalnummer = 655021 }, new Mitarbeiter() { Name = "Silke Zimmer", Personalnummer = 437785 }, new Mitarbeiter() { Name = "Antje Hoch", Personalnummer = 414790 }, new Mitarbeiter() { Name = "Selina Grünewald", Personalnummer = 554280 }, new Mitarbeiter() { Name = "Jennifer Eggers", Personalnummer = 776081 }, new Mitarbeiter() { Name = "Kerstin Hertz", Personalnummer = 287285 }, new Mitarbeiter() { Name = "Torsten Lehmann", Personalnummer = 163138 }, new Mitarbeiter() { Name = "Sophie Hertzog", Personalnummer = 437856 }, } }
            };

            List<Firma> fnew1 = new List<Firma>
            {
                new Firma{Plz=2620, Ort="Neunkirchen", Name="BASF Chemicals", Mitarbeiteranzahl=1503, Umsatz=12502005.51, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Niklas Schroder", Personalnummer=724415 }, new Mitarbeiter() { Name = "Uwe Fink", Personalnummer = 250597 }, new Mitarbeiter() { Name = "Florian Lange", Personalnummer = 214495 }, new Mitarbeiter() { Name = "Sabrina Kaiser", Personalnummer = 839439 }, new Mitarbeiter() { Name = "Peter Schröder", Personalnummer = 809031 }, new Mitarbeiter() { Name = "Marie Meier", Personalnummer = 552148 }, new Mitarbeiter() { Name = "Katharina Finkel", Personalnummer = 303581 }, new Mitarbeiter() { Name = "Ralph Krüger", Personalnummer = 541461 }, new Mitarbeiter() { Name = "Ulrike Nacht", Personalnummer = 390255 }, new Mitarbeiter() { Name = "Christian Wechsler", Personalnummer = 392697 }, } },
                new Firma{Plz=3910, Ort="Zwettl", Name="Kastner GmbH", Mitarbeiteranzahl=980, Umsatz=5602502.63, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Frank Kohler", Personalnummer= 701927 }, new Mitarbeiter() { Name = "Doreen Winkel", Personalnummer = 572297 }, new Mitarbeiter() { Name = "Steffen Lang", Personalnummer = 612919 }, new Mitarbeiter() { Name = "Max Schradin", Personalnummer = 422706 }, new Mitarbeiter() { Name = "Anna Heesch", Personalnummer = 421759 }, new Mitarbeiter() { Name = "Tina Kaiser", Personalnummer = 937024 }, new Mitarbeiter() { Name = "Sophia Vogel", Personalnummer = 690567 }, new Mitarbeiter() { Name = "Sarah Schuster", Personalnummer = 579211 }, new Mitarbeiter() { Name = "Maximillian König", Personalnummer = 400532 }, new Mitarbeiter() { Name = "Markus Meier", Personalnummer = 553076 }, } },
                new Firma{Plz=3943, Ort="Schrems", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=1143, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Lena Weißmüller", Personalnummer= 106377 }, new Mitarbeiter() { Name = "Kerstin Eberhardt", Personalnummer = 888195 }, new Mitarbeiter() { Name = "Tanja Schultz", Personalnummer = 754852 }, new Mitarbeiter() { Name = "Jörg Metzger", Personalnummer = 760720 }, new Mitarbeiter() { Name = "Thomas Schürmann", Personalnummer = 880910 }, new Mitarbeiter() { Name = "Jürgen Milski", Personalnummer = 938344 }, new Mitarbeiter() { Name = "Matthias Brandt", Personalnummer = 109448 }, new Mitarbeiter() { Name = "Jessica Ziegler", Personalnummer = 937530 }, new Mitarbeiter() { Name = "Robert Achen", Personalnummer = 521639 }, new Mitarbeiter() { Name = "Swen Richter", Personalnummer = 577936 }, } },
                new Firma{Plz=6460, Ort="Imst", Name="Zirbenholztischlerei Riml", Mitarbeiteranzahl=12, Umsatz=50025.55, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Tim Friedmann", Personalnummer= 503341 }, new Mitarbeiter() { Name = "Franziska Gerbert", Personalnummer = 299716 }, new Mitarbeiter() { Name = "Marie Hahn", Personalnummer = 278794 }, new Mitarbeiter() { Name = "Juliane Wernher", Personalnummer = 782524 }, new Mitarbeiter() { Name = "Kathrin Sankt", Personalnummer = 589204 }, new Mitarbeiter() { Name = "Uta Wolf", Personalnummer = 205980 }, new Mitarbeiter() { Name = "Michael Böhminger", Personalnummer = 523707 }, new Mitarbeiter() { Name = "Bernd Kaufmann", Personalnummer = 166427 }, new Mitarbeiter() { Name = "Frank Zimmer", Personalnummer = 804801 }, new Mitarbeiter() { Name = "Daniela Sonntag", Personalnummer = 586786 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=555, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Nicole Böhm", Personalnummer= 829483 }, new Mitarbeiter() { Name = "Franziska Gerber", Personalnummer = 993372 }, new Mitarbeiter() { Name = "Jan Amsel", Personalnummer = 573213 }, new Mitarbeiter() { Name = "Marco Schmidt", Personalnummer = 995896 }, new Mitarbeiter() { Name = "Manuela Winkel", Personalnummer = 619780 }, new Mitarbeiter() { Name = "Maik Berg", Personalnummer = 728071 }, new Mitarbeiter() { Name = "Antje Neumann", Personalnummer = 155479 }, new Mitarbeiter() { Name = "Stephanie Peters", Personalnummer = 221289 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 575835 }, new Mitarbeiter() { Name = "Maik Hübner", Personalnummer = 437003 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Pizzeria San Marco", Mitarbeiteranzahl=3, Umsatz=78526.02, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Simone Jäger", Personalnummer= 977047 }, new Mitarbeiter() { Name = "Jürgen Müller", Personalnummer = 554905 }, new Mitarbeiter() { Name = "Annett Orner", Personalnummer = 296987 }, new Mitarbeiter() { Name = "Anja Nussbaum", Personalnummer = 558412 }, new Mitarbeiter() { Name = "Tanja Seiler", Personalnummer = 272545 }, new Mitarbeiter() { Name = "Maximillian Baumgartner", Personalnummer = 395430 }, new Mitarbeiter() { Name = "Stephanie Eisenberg", Personalnummer = 140201 }, new Mitarbeiter() { Name = "Barbara Decker", Personalnummer = 613096 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 864056 }, new Mitarbeiter() { Name = "Lukas Jäger", Personalnummer = 798737 }, } },
                new Firma{Plz=9800, Ort="Spittal an der Drau", Name="Mineralöle Leitner", Mitarbeiteranzahl=6, Umsatz=96325.12, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Maik Ehrlichmann", Personalnummer= 327798 }, new Mitarbeiter() { Name = "Leon Schuster", Personalnummer = 735626 }, new Mitarbeiter() { Name = "Torsten Traugott", Personalnummer = 687167 }, new Mitarbeiter() { Name = "Martina Bayer", Personalnummer = 259549 }, new Mitarbeiter() { Name = "Katja Münch", Personalnummer = 470189 }, new Mitarbeiter() { Name = "Dirk Altmann", Personalnummer = 734471 }, new Mitarbeiter() { Name = "Ulrich Lehrer", Personalnummer = 959922 }, new Mitarbeiter() { Name = "Bernd Eiffel", Personalnummer = 275658 }, new Mitarbeiter() { Name = "Matthias Brauer", Personalnummer = 986830 }, new Mitarbeiter() { Name = "Manuela Fischer", Personalnummer = 434674 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Fahrschule Pannonia", Mitarbeiteranzahl=22, Umsatz=101893.98, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Sarah Hermann", Personalnummer= 929309 }, new Mitarbeiter() { Name = "Martin Lang", Personalnummer = 509057 }, new Mitarbeiter() { Name = "Anke Frühauf", Personalnummer = 845379 }, new Mitarbeiter() { Name = "Lucas Langmann", Personalnummer = 464069 }, new Mitarbeiter() { Name = "Dieter Kohler", Personalnummer = 209170 }, new Mitarbeiter() { Name = "Juliane Blau", Personalnummer = 399676 }, new Mitarbeiter() { Name = "Andrea Gruber", Personalnummer = 320523 }, new Mitarbeiter() { Name = "Benjamin Sommer", Personalnummer = 485817 }, new Mitarbeiter() { Name = "Ernestine Bleich", Personalnummer = 555064 }, new Mitarbeiter() { Name = "Jens Fenstermacher", Personalnummer = 495463 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=541, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Michelle Kuhn", Personalnummer= 223838 }, new Mitarbeiter() { Name = "Niklas Goldschmidt", Personalnummer = 797250 }, new Mitarbeiter() { Name = "Stephanie Bergmann", Personalnummer = 655021 }, new Mitarbeiter() { Name = "Silke Zimmer", Personalnummer = 437785 }, new Mitarbeiter() { Name = "Antje Hoch", Personalnummer = 414790 }, new Mitarbeiter() { Name = "Selina Grünewald", Personalnummer = 554280 }, new Mitarbeiter() { Name = "Jennifer Eggers", Personalnummer = 776081 }, new Mitarbeiter() { Name = "Kerstin Hertz", Personalnummer = 287285 }, new Mitarbeiter() { Name = "Torsten Lehmann", Personalnummer = 163138 }, new Mitarbeiter() { Name = "Sophie Hertzog", Personalnummer = 437856 }, } }
            };

            List<Firma> fnew2 = new List<Firma>
            {
                new Firma{Plz=2620, Ort="Neunkirchen", Name="BASF Chemicals", Mitarbeiteranzahl=1503, Umsatz=12502005.51, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Niklas Schroder", Personalnummer=724415 }, new Mitarbeiter() { Name = "Uwe Fink", Personalnummer = 250597 }, new Mitarbeiter() { Name = "Florian Lange", Personalnummer = 214495 }, new Mitarbeiter() { Name = "Sabrina Kaiser", Personalnummer = 839439 }, new Mitarbeiter() { Name = "Peter Schröder", Personalnummer = 809031 }, new Mitarbeiter() { Name = "Marie Meier", Personalnummer = 552148 }, new Mitarbeiter() { Name = "Katharina Finkel", Personalnummer = 303581 }, new Mitarbeiter() { Name = "Ralph Krüger", Personalnummer = 541461 }, new Mitarbeiter() { Name = "Ulrike Nacht", Personalnummer = 390255 }, new Mitarbeiter() { Name = "Christian Wechsler", Personalnummer = 392697 }, } },
                new Firma{Plz=3910, Ort="Zwettl", Name="Kastner GmbH", Mitarbeiteranzahl=980, Umsatz=5602502.63, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Frank Kohler", Personalnummer= 701927 }, new Mitarbeiter() { Name = "Doreen Winkel", Personalnummer = 572297 }, new Mitarbeiter() { Name = "Steffen Lang", Personalnummer = 612919 }, new Mitarbeiter() { Name = "Max Schradin", Personalnummer = 422706 }, new Mitarbeiter() { Name = "Anna Heesch", Personalnummer = 421759 }, new Mitarbeiter() { Name = "Tina Kaiser", Personalnummer = 937024 }, new Mitarbeiter() { Name = "Sophia Vogel", Personalnummer = 690567 }, new Mitarbeiter() { Name = "Sarah Schuster", Personalnummer = 579211 }, new Mitarbeiter() { Name = "Maximillian König", Personalnummer = 400532 }, new Mitarbeiter() { Name = "Markus Meier", Personalnummer = 553076 }, } },
                new Firma{Plz=3943, Ort="Schrems", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=1143, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Lena Weißmüller", Personalnummer= 106377 }, new Mitarbeiter() { Name = "Kerstin Eberhardt", Personalnummer = 888195 }, new Mitarbeiter() { Name = "Tanja Schultz", Personalnummer = 754852 }, new Mitarbeiter() { Name = "Jörg Metzger", Personalnummer = 760720 }, new Mitarbeiter() { Name = "Thomas Schürmann", Personalnummer = 880910 }, new Mitarbeiter() { Name = "Jürgen Milski", Personalnummer = 938344 }, new Mitarbeiter() { Name = "Matthias Brandt", Personalnummer = 109448 }, new Mitarbeiter() { Name = "Jessica Ziegler", Personalnummer = 937530 }, new Mitarbeiter() { Name = "Robert Achen", Personalnummer = 521639 }, new Mitarbeiter() { Name = "Swen Richter", Personalnummer = 577936 }, } },
                new Firma{Plz=6460, Ort="Imst", Name="Zirbenholztischlerei Riml", Mitarbeiteranzahl=12, Umsatz=50025.55, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Tim Friedmann", Personalnummer= 503341 }, new Mitarbeiter() { Name = "Franziska Gerbert", Personalnummer = 299716 }, new Mitarbeiter() { Name = "Marie Hahn", Personalnummer = 278794 }, new Mitarbeiter() { Name = "Juliane Wernher", Personalnummer = 782524 }, new Mitarbeiter() { Name = "Kathrin Sankt", Personalnummer = 589204 }, new Mitarbeiter() { Name = "Uta Wolf", Personalnummer = 205980 }, new Mitarbeiter() { Name = "Michael Böhminger", Personalnummer = 523707 }, new Mitarbeiter() { Name = "Bernd Kaufmann", Personalnummer = 166427 }, new Mitarbeiter() { Name = "Frank Zimmer", Personalnummer = 804801 }, new Mitarbeiter() { Name = "Daniela Sonntag", Personalnummer = 586786 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=555, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Nicole Böhm", Personalnummer= 829483 }, new Mitarbeiter() { Name = "Franziska Gerber", Personalnummer = 993372 }, new Mitarbeiter() { Name = "Jan Amsel", Personalnummer = 573213 }, new Mitarbeiter() { Name = "Marco Schmidt", Personalnummer = 995896 }, new Mitarbeiter() { Name = "Manuela Winkel", Personalnummer = 619780 }, new Mitarbeiter() { Name = "Maik Berg", Personalnummer = 728071 }, new Mitarbeiter() { Name = "Antje Neumann", Personalnummer = 155479 }, new Mitarbeiter() { Name = "Stephanie Peters", Personalnummer = 221289 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 575835 }, new Mitarbeiter() { Name = "Maik Hübner", Personalnummer = 437003 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Pizzeria San Marco", Mitarbeiteranzahl=3, Umsatz=78526.02, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Simone Jäger", Personalnummer= 977047 }, new Mitarbeiter() { Name = "Jürgen Müller", Personalnummer = 554905 }, new Mitarbeiter() { Name = "Annett Orner", Personalnummer = 296987 }, new Mitarbeiter() { Name = "Anja Nussbaum", Personalnummer = 558412 }, new Mitarbeiter() { Name = "Tanja Seiler", Personalnummer = 272545 }, new Mitarbeiter() { Name = "Maximillian Baumgartner", Personalnummer = 395430 }, new Mitarbeiter() { Name = "Stephanie Eisenberg", Personalnummer = 140201 }, new Mitarbeiter() { Name = "Barbara Decker", Personalnummer = 613096 }, new Mitarbeiter() { Name = "Frank Weiß", Personalnummer = 864056 }, new Mitarbeiter() { Name = "Lukas Jäger", Personalnummer = 798737 }, } },
                new Firma{Plz=9800, Ort="Spittal an der Drau", Name="Mineralöle Leitner", Mitarbeiteranzahl=6, Umsatz=96325.12, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Maik Ehrlichmann", Personalnummer= 327798 }, new Mitarbeiter() { Name = "Leon Schuster", Personalnummer = 735626 }, new Mitarbeiter() { Name = "Torsten Traugott", Personalnummer = 687167 }, new Mitarbeiter() { Name = "Martina Bayer", Personalnummer = 259549 }, new Mitarbeiter() { Name = "Katja Münch", Personalnummer = 470189 }, new Mitarbeiter() { Name = "Dirk Altmann", Personalnummer = 734471 }, new Mitarbeiter() { Name = "Ulrich Lehrer", Personalnummer = 959922 }, new Mitarbeiter() { Name = "Bernd Eiffel", Personalnummer = 275658 }, new Mitarbeiter() { Name = "Matthias Brauer", Personalnummer = 986830 }, new Mitarbeiter() { Name = "Manuela Fischer", Personalnummer = 434674 }, } },
                new Firma{Plz=7100, Ort="Neusiedl am See", Name="Fahrschule Pannonia", Mitarbeiteranzahl=22, Umsatz=101893.98, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Sarah Hermann", Personalnummer= 929309 }, new Mitarbeiter() { Name = "Martin Lang", Personalnummer = 509057 }, new Mitarbeiter() { Name = "Anke Frühauf", Personalnummer = 845379 }, new Mitarbeiter() { Name = "Lucas Langmann", Personalnummer = 464069 }, new Mitarbeiter() { Name = "Dieter Kohler", Personalnummer = 209170 }, new Mitarbeiter() { Name = "Juliane Blau", Personalnummer = 399676 }, new Mitarbeiter() { Name = "Andrea Gruber", Personalnummer = 320523 }, new Mitarbeiter() { Name = "Benjamin Sommer", Personalnummer = 485817 }, new Mitarbeiter() { Name = "Ernestine Bleich", Personalnummer = 555064 }, new Mitarbeiter() { Name = "Jens Fenstermacher", Personalnummer = 495463 }, } },
                new Firma{Plz=3902, Ort="Vitis", Name="ELK Fertigteilhaus", Mitarbeiteranzahl=541, Umsatz=11453269.25, M= new List<Mitarbeiter>{ new Mitarbeiter() { Name= "Michelle Kuhn", Personalnummer= 223838 }, new Mitarbeiter() { Name = "Niklas Goldschmidt", Personalnummer = 797250 }, new Mitarbeiter() { Name = "Stephanie Bergmann", Personalnummer = 655021 }, new Mitarbeiter() { Name = "Silke Zimmer", Personalnummer = 437785 }, new Mitarbeiter() { Name = "Antje Hoch", Personalnummer = 414790 }, new Mitarbeiter() { Name = "Selina Grünewald", Personalnummer = 554280 }, new Mitarbeiter() { Name = "Jennifer Eggers", Personalnummer = 776081 }, new Mitarbeiter() { Name = "Kerstin Hertz", Personalnummer = 287285 }, new Mitarbeiter() { Name = "Torsten Lehmann", Personalnummer = 163138 }, new Mitarbeiter() { Name = "Sophie Hertzog", Personalnummer = 437856 }, } }
            };

            f.Sort((Firma f1, Firma f2) =>
            {
                return f1.Name.CompareTo(f2.Name);
            });

            f.Sort();

            List<Firma> ffindall = f.FindAll((Firma ff) =>
            {
                return ff.Ort == "Zwettl";
            });

            List<Firma> fwhere = new List<Firma>();
            foreach (Firma fhelp in f.Where(fhelp => (fhelp.Ort.Substring(0, 1) == "A")||(fhelp.Ort.Substring(0, 1) == "Z")))
            {
                fwhere.Add(fhelp);
            }

            Boolean fexists = false;

            if(f.Exists(fhelp => fhelp.Mitarbeiteranzahl > 100))
            {
                fexists = true;
            }

            //-------------------------------------------------------------------------------------------------------------------

            foreach (Firma fhelp in f)
            {
                Console.WriteLine(fhelp.Name + " " + fhelp.Umsatz);
            }

            Zuwachs(ref f);

            Console.WriteLine(" ");

            foreach (Firma fhelp in f)
            {
                Console.WriteLine(fhelp.Name + " " + fhelp.Umsatz);
            }

            //Die Werte werden nach dem Methodenaufruf geändert, weil die Liste dauerhaft mit dem ref-Operator geändert wird.

            Firma fumsatzstark = Umsatzstark(f);

            fumsatzstark.Plz = 3270;
            fumsatzstark.Ort = "Scheibbs";

            Console.WriteLine(" ");

            foreach (Firma fhelp in f)
            {
                Console.WriteLine(fhelp.Name + " " + fhelp.Umsatz + " " + fhelp.Plz + " " + fhelp.Ort);
            }

            //Die Postleitzahl und der Ort dieser Firma hat sich geändert, weil die Methode 'Umsatzstark' statisch ist.

            //Der Benutzer kann durch Eingabe einer PLZ, alle betroffenen Firmeneinträge aus der Liste entfernen:

            PlzEntfernung(ref f);

            Console.WriteLine(" ");

            foreach (Firma fhelp in f)
            {
                Console.WriteLine(fhelp.Name + " " + fhelp.Umsatz + " " + fhelp.Plz + " " + fhelp.Ort);
            }

            //Sortiere die Liste alphabetisch absteigend, bei zwei gleichen Firmennamen sollen diese nach der PLZ aufsteigend sortiert werden. Sind auch die beiden PLZ gleich, dann wird absteigend nach Mitarbeiter sortiert. Überprüfe die Sortierung mit geeigneten Testdaten:

            f.Sort((Firma f1, Firma f2) =>
            {
                if(f1.Name == f2.Name)
                {
                    if(f1.Plz == f2.Plz)
                    {
                        return f2.Mitarbeiteranzahl.CompareTo(f1.Mitarbeiteranzahl);
                    }

                    return f1.Plz.CompareTo(f2.Plz);
                }

                return f2.Name.CompareTo(f1.Name);
            });

            Console.WriteLine(" ");

            foreach (Firma fhelp in f)
            {
                Console.WriteLine(fhelp.Name + " " + fhelp.Umsatz + " " + fhelp.Plz + " " + fhelp.Ort + " " + fhelp.Mitarbeiteranzahl);
            }

            //-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            Console.WriteLine(" ");
            Console.WriteLine("DRITTE ÜBUNG");
            Console.WriteLine(" ");

            List<Firma> fq = new List<Firma>(fnew);
            

            //fq[2] = new Firma { Plz = 9971, Ort="Matrei in Osttirol", Name="Alpengasthof Venedigerhaus", Mitarbeiteranzahl=22, Umsatz=8632569.91, M = new List<Mitarbeiter> { new Mitarbeiter() { Name = "Henry Berger", Personalnummer = 563299 }, new Mitarbeiter() { Name = "Elias Senner", Personalnummer = 556145 }, new Mitarbeiter() { Name = "Hermine von der Aistweide", Personalnummer = 936762 }, new Mitarbeiter() { Name = "Paola Maria", Personalnummer = 129642 }, new Mitarbeiter() { Name = "Kilian Heinrich", Personalnummer = 456456 }, new Mitarbeiter() { Name = "Simon Wiefels", Personalnummer = 754125 }, new Mitarbeiter() { Name = "Rainer Winkler", Personalnummer = 584512 }, new Mitarbeiter() { Name = "Jonas Ems", Personalnummer = 854541 }, new Mitarbeiter() { Name = "Yvonne zur Mühlen", Personalnummer = 845954 }, new Mitarbeiter() { Name = "Rolf Wolf", Personalnummer = 987454 }, } };



            fq[2].Plz = 9971;
            fq[2].Ort = "Matrei in Osttirol";
            fq[2].M = new List<Mitarbeiter> { new Mitarbeiter() { Name = "Henry Berger", Personalnummer = 563299 }, new Mitarbeiter() { Name = "Elias Senner", Personalnummer = 556145 }, new Mitarbeiter() { Name = "Hermine von der Aistweide", Personalnummer = 936762 }, new Mitarbeiter() { Name = "Paola Maria", Personalnummer = 129642 }, new Mitarbeiter() { Name = "Kilian Heinrich", Personalnummer = 456456 }, new Mitarbeiter() { Name = "Simon Wiefels", Personalnummer = 754125 }, new Mitarbeiter() { Name = "Rainer Winkler", Personalnummer = 584512 }, new Mitarbeiter() { Name = "Jonas Ems", Personalnummer = 854541 }, new Mitarbeiter() { Name = "Yvonne zur Mühlen", Personalnummer = 845954 }, new Mitarbeiter() { Name = "Rolf Wolf", Personalnummer = 987454 }, };

            Console.WriteLine("SHALLOW COPY");
            Console.WriteLine("Original-Liste:");
            Console.WriteLine(" ");

            foreach (Firma fhelp in fnew)
            {
                Console.WriteLine(fhelp);
                Console.WriteLine(" ");
            }

            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Copy-Liste:");
            Console.WriteLine(" ");

            foreach (Firma fhelp in fq)
            {
                Console.WriteLine(fhelp);
                Console.WriteLine(" ");
            }

            List<Firma> fs = new List<Firma>();


            //List < Mitarbeiter > ml = new List<Mitarbeiter>();





            //foreach (Firma fhelp in f)
            //{
            //    //fhelp.M = new List<Mitarbeiter>(fhelp.M);
            //    fs.Add((Firma)fhelp.ShallowDeepCopy());
            //    //fhelp.M = new List<Mitarbeiter>(fhelp.M);
            //}

            for (int i = 0; i < fs.Count-1; i++)
            {
                fs[i].Plz = f[i].Plz;
                fs[i].Ort = f[i].Ort;
                fs[i].Name = f[i].Name;
                fs[i].Mitarbeiteranzahl = f[i].Mitarbeiteranzahl;
                fs[i].Umsatz = f[i].Umsatz;
            }

            foreach (Firma fhelp in fnew2)
            {
                fs.Add((Firma)fhelp.Clone());
            }



            //for (int i = 0; i < f.Count; i++)
            //{
            //    fs[i].M = f[i].M;
            //}

            fs[2].Plz = 9971;
            fs[2].Ort = "Matrei in Osttirol";
            fs[2].M = new List<Mitarbeiter> { new Mitarbeiter() { Name = "Henry Berger", Personalnummer = 563299 }, new Mitarbeiter() { Name = "Elias Senner", Personalnummer = 556145 }, new Mitarbeiter() { Name = "Hermine von der Aistweide", Personalnummer = 936762 }, new Mitarbeiter() { Name = "Paola Maria", Personalnummer = 129642 }, new Mitarbeiter() { Name = "Kilian Heinrich", Personalnummer = 456456 }, new Mitarbeiter() { Name = "Simon Wiefels", Personalnummer = 754125 }, new Mitarbeiter() { Name = "Rainer Winkler", Personalnummer = 584512 }, new Mitarbeiter() { Name = "Jonas Ems", Personalnummer = 854541 }, new Mitarbeiter() { Name = "Yvonne zur Mühlen", Personalnummer = 845954 }, new Mitarbeiter() { Name = "Rolf Wolf", Personalnummer = 987454 }, };

            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("DEEP COPY & SHALLOW COPY MITARBEITER");
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Original-Liste:");
            Console.WriteLine(" ");

            foreach (Firma fhelp in fnew2)
            {
                Console.WriteLine(fhelp);
                Console.WriteLine(" ");
            }

            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Copy-Liste:");
            Console.WriteLine(" ");

            foreach (Firma fhelp in fs)
            {
                Console.WriteLine(fhelp);
                Console.WriteLine(" ");
            }

            //List<Firma> fx = (List<Firma>)Clone(f);

            List<Firma> fx = new List<Firma>();

            foreach (Firma fhelp in fnew1)
            {
                fx.Add((Firma)fhelp.Clone());
            }

            fx[2].Plz = 9971;
            fx[2].Ort = "Matrei in Osttirol";
            fx[2].M = new List<Mitarbeiter> { new Mitarbeiter() { Name = "Henry Berger", Personalnummer = 563299 }, new Mitarbeiter() { Name = "Elias Senner", Personalnummer = 556145 }, new Mitarbeiter() { Name = "Hermine von der Aistweide", Personalnummer = 936762 }, new Mitarbeiter() { Name = "Paola Maria", Personalnummer = 129642 }, new Mitarbeiter() { Name = "Kilian Heinrich", Personalnummer = 456456 }, new Mitarbeiter() { Name = "Simon Wiefels", Personalnummer = 754125 }, new Mitarbeiter() { Name = "Rainer Winkler", Personalnummer = 584512 }, new Mitarbeiter() { Name = "Jonas Ems", Personalnummer = 854541 }, new Mitarbeiter() { Name = "Yvonne zur Mühlen", Personalnummer = 845954 }, new Mitarbeiter() { Name = "Rolf Wolf", Personalnummer = 987454 }, };

            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("DEEP COPY VON ALLEM");
            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Original-Liste:");
            Console.WriteLine(" ");

            foreach (Firma fhelp in fnew1)
            {
                Console.WriteLine(fhelp);
                Console.WriteLine(" ");
            }

            Console.WriteLine(" ");
            Console.WriteLine(" ");
            Console.WriteLine("Copy-Liste:");
            Console.WriteLine(" ");

            foreach (Firma fhelp in fx)
            {
                Console.WriteLine(fhelp);
                Console.WriteLine(" ");
            }

            //Nummer 6: Die Aufgaben wurden überprüft, indem in der kopierten Firmenliste etwas geändert wurde. So kann man feststellen, ob das Shallow-Copy oder das Deep-Copy funktioniert hat.

            Console.ReadLine();
        }

        static void Zuwachs (ref List<Firma> f)
        {
            foreach (Firma fhelp in f)
            {
                if (fhelp.Ort == "Zwettl")
                {
                    fhelp.Umsatz = fhelp.Umsatz * 1.1;
                }
            }
        }

        static Firma Umsatzstark (List<Firma> fl)
        {
            fl.Sort((Firma f1, Firma f2) =>
            {
                return f1.Umsatz.CompareTo(f2.Umsatz);
            });

            return fl[fl.Count-1];
        }

        static void PlzEntfernung (ref List<Firma> f)
        {
            Console.WriteLine(" ");
            Console.WriteLine("Geben Sie die zu entfernende Postleitzahl ein: ");

            int plzentf = 0;

            try
            {
                plzentf = Convert.ToInt32(Console.ReadLine());
            }
            catch
            {
                Console.WriteLine("Sie haben keine richtige Postleitzahl eingegeben! ");
            }

            for (int i = f.Count - 1; i >= 0; --i)
            {
                if (f[i].Plz == plzentf)
                {
                    f.RemoveAt(i);
                }
            }
        }

        //static public object Clone(List<Firma> f)
        //{
        //    //List<Mitarbeiter> mclone = new List<Mitarbeiter>(this.M);List<Firma> fclone = new List<Firma> { Plz = Firma.Plz, Ort = this.Ort, Name = this.Name, Mitarbeiteranzahl = this.Mitarbeiteranzahl, Umsatz = this.Umsatz, M = mclone };
        //    //return fclone;

        //    List<Firma> fclone = new List<Firma>();

        //    foreach (Firma fhelp in f)
        //    {
        //        fclone.Add(fhelp);
        //    }
        //    return fclone;
        //}
    }
}
