﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerischeListeExtended
{
    class Firma:IComparable/*, ICloneable*/
    {
        public Int32 Plz { get; set; }
        public String Ort { get; set; }
        public String Name { get; set; }
        public Int32 Mitarbeiteranzahl { get; set; }
        public Double Umsatz { get; set; }

        public List<Mitarbeiter> M { get; set; }
        //Die Mitarbeiteranzahl ist dadurch unnötig geworden, weil man die Mitarbeiter in der Mitarbeiterliste zählen kann.

        public int CompareTo(object obj)
        {
            Firma fhelp = (Firma)obj;

            if (this.Name == fhelp.Name)
            {
                return this.Plz.CompareTo(fhelp.Plz);
            }
            else
            {
                return this.Name.CompareTo(fhelp.Name);
            }
        }

        public override string ToString()
        {
            return this.Plz + " " + this.Ort + " " + this.Name + " " + this.Umsatz + " Mitarbeiter: " + string.Join(", ", this.M);
        }

        public object ShallowCopy()
        {
            return this.MemberwiseClone();
        }

        public object Clone()
        {
            List<Mitarbeiter> mclone = new List<Mitarbeiter>(M);
            Firma fclone = new Firma { Plz = this.Plz, Ort = this.Ort, Name = this.Name, Mitarbeiteranzahl = this.Mitarbeiteranzahl, Umsatz = this.Umsatz, M =  mclone};
            return fclone;
        }

        //return fclone;

        //public object Clone()
        //{

        //    Firma fclone = new Firma { Plz = this.Plz, Ort = this.Ort, Name = this.Name, Mitarbeiteranzahl = this.Mitarbeiteranzahl, Umsatz = this.Umsatz, M = this.M };
        //    return fclone;
        //}

        //public List<Mitarbeiter> ma = new List<Mitarbeiter>();

        public Firma ShallowDeepCopy()
        {
            List<Mitarbeiter> ma1 = new List<Mitarbeiter>(this.M);
            Firma f = new Firma { Plz = this.Plz, Ort = this.Ort, Name = this.Name, Mitarbeiteranzahl = this.Mitarbeiteranzahl, Umsatz = this.Umsatz, M = ma1 };
            return f;
        }
    }
}
