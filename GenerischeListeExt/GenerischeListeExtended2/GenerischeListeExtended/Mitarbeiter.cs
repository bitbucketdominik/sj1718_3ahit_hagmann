﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerischeListeExtended
{
    class Mitarbeiter
    {
        public String Name { get; set; }
        public Int32 Personalnummer { get; set; }

        public override string ToString()
        {
            return this.Name + " " + this.Personalnummer;
        }

        public object ShallowCopy()
        {
            return this.MemberwiseClone();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
